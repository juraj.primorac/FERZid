﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class VrstaUredaja
    {
        public VrstaUredaja()
        {
            Uredaj = new HashSet<Uredaj>();
        }

        public int IdvrsteUredaja { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }

        public virtual ICollection<Uredaj> Uredaj { get; set; }
    }
}
