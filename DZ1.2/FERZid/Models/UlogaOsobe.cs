﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FERZid.Models
{
    public partial class UlogaOsobe
    {
        public int IdulogeOsobe { get; set; }

        [Required]
        [Display(Name = "Vrsta uloge")]
        public int IdvrsteUloge { get; set; }

        [Required]
        [Display(Name = "Osoba")]
        public int Idosobe { get; set; }

        [Required]
        [Display(Name = "Sadržaj")]
        public int Idsadržaja { get; set; }

        public virtual Osoba IdosobeNavigation { get; set; }
        public virtual Sadržaj IdsadržajaNavigation { get; set; }
        public virtual VrstaUloge IdvrsteUlogeNavigation { get; set; }
    }
}
