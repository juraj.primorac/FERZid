﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class StatusServisa
    {
        public StatusServisa()
        {
            Servis = new HashSet<Servis>();
        }

        public int Idstatusa { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }

        public virtual ICollection<Servis> Servis { get; set; }
    }
}
