﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class SadrzajNaSegmentu
    {
        public int IdsnaS { get; set; }
        public int Idsegmenta { get; set; }
        public int Idsadrzaja { get; set; }

        public virtual Sadržaj IdsadrzajaNavigation { get; set; }
        public virtual Segment IdsegmentaNavigation { get; set; }
    }
}
