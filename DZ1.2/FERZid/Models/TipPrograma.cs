﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class TipPrograma
    {
        public TipPrograma()
        {
            Program = new HashSet<Program>();
        }

        public int IdTipaPrograma { get; set; }
        public string NazivTipaPrograma { get; set; }
        public string Kontekst { get; set; }

        public virtual ICollection<Program> Program { get; set; }
    }
}
