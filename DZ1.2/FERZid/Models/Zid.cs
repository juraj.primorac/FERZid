﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class Zid
    {
        public Zid()
        {
            ProgramAktivan = new HashSet<ProgramAktivan>();
            Segment = new HashSet<Segment>();
        }

        public int Idzida { get; set; }
        public DateTime DatumPostavljanja { get; set; }
        public string Lokacija { get; set; }
        public string Opis { get; set; }
        public int Visina { get; set; }
        public int Sirina { get; set; }

        public virtual ICollection<ProgramAktivan> ProgramAktivan { get; set; }
        public virtual ICollection<Segment> Segment { get; set; }
    }
}
