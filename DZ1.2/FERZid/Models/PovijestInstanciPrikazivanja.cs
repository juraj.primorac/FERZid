﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class PovijestInstanciPrikazivanja
    {
        public PovijestInstanciPrikazivanja()
        {
            PovijestPrikazivanja = new HashSet<PovijestPrikazivanja>();
        }

        public DateTime? VrijemePrikazivanja { get; set; }
        public int Idsadržaja { get; set; }
        public int Idzida { get; set; }
        public int IdInstancePrikazivanja { get; set; }

        public virtual ICollection<PovijestPrikazivanja> PovijestPrikazivanja { get; set; }
        public virtual Sadržaj IdsadržajaNavigation { get; set; }
    }
}
