﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class Servis
    {
        public Servis()
        {
            UlogaServisera = new HashSet<UlogaServisera>();
        }

        public int Idservisa { get; set; }
        public int Iduredaja { get; set; }
        public string Opis { get; set; }
        public int Idstatusa { get; set; }
        public int Idvrste { get; set; }
        public DateTime Pocetak { get; set; }
        public DateTime Zavrsetak { get; set; }

        public virtual ICollection<UlogaServisera> UlogaServisera { get; set; }
        public virtual StatusServisa IdstatusaNavigation { get; set; }
        public virtual Uredaj IduredajaNavigation { get; set; }
        public virtual VrstaServisa IdvrsteNavigation { get; set; }
    }
}
