﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class SadrzajPrograma
    {
        public int IdPrograma { get; set; }
        public int IdSadrzaja { get; set; }

        public virtual Program IdProgramaNavigation { get; set; }
        public virtual Sadržaj IdSadrzajaNavigation { get; set; }
    }
}
