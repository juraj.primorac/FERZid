﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class Serviser
    {
        public Serviser()
        {
            UlogaServisera = new HashSet<UlogaServisera>();
        }

        public int Idservisera { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Oib { get; set; }
        public string Email { get; set; }
        public string Sjediste { get; set; }
        public string Ziroracun { get; set; }
        public int Idbanke { get; set; }

        public virtual ICollection<UlogaServisera> UlogaServisera { get; set; }
        public virtual Banka IdbankeNavigation { get; set; }
    }
}
