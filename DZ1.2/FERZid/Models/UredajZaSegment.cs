﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class UredajZaSegment
    {
        public int Iduredaja { get; set; }
        public int Idsegmenta { get; set; }

        public virtual Segment IdsegmentaNavigation { get; set; }
        public virtual Uredaj IduredajaNavigation { get; set; }
    }
}
