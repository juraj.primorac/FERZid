﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FERZid.Models
{
    public partial class VrstaUloge
    {
        public VrstaUloge()
        {
            UlogaOsobe = new HashSet<UlogaOsobe>();
        }

        public int IdvrsteUloge { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Naziv { get; set; }

        public virtual ICollection<UlogaOsobe> UlogaOsobe { get; set; }
    }
}
