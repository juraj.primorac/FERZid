﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FERZid.Models
{
    public partial class RPPP08Context : DbContext
    {
        public virtual DbSet<Banka> Banka { get; set; }
        public virtual DbSet<Format> Format { get; set; }
        public virtual DbSet<Komponenta> Komponenta { get; set; }
        public virtual DbSet<Osoba> Osoba { get; set; }
        public virtual DbSet<PovijestInstanciPrikazivanja> PovijestInstanciPrikazivanja { get; set; }
        public virtual DbSet<PovijestPrikazivanja> PovijestPrikazivanja { get; set; }
        public virtual DbSet<Program> Program { get; set; }
        public virtual DbSet<ProgramAktivan> ProgramAktivan { get; set; }
        public virtual DbSet<SadrzajNaSegmentu> SadrzajNaSegmentu { get; set; }
        public virtual DbSet<SadrzajPrograma> SadrzajPrograma { get; set; }
        public virtual DbSet<Sadržaj> Sadržaj { get; set; }
        public virtual DbSet<Segment> Segment { get; set; }
        public virtual DbSet<Servis> Servis { get; set; }
        public virtual DbSet<Serviser> Serviser { get; set; }
        public virtual DbSet<StatusServisa> StatusServisa { get; set; }
        public virtual DbSet<TipKomponente> TipKomponente { get; set; }
        public virtual DbSet<TipPrograma> TipPrograma { get; set; }
        public virtual DbSet<UlogaOsobe> UlogaOsobe { get; set; }
        public virtual DbSet<UlogaServisera> UlogaServisera { get; set; }
        public virtual DbSet<Uredaj> Uredaj { get; set; }
        public virtual DbSet<UredajZaSegment> UredajZaSegment { get; set; }
        public virtual DbSet<VrstaSadržaja> VrstaSadržaja { get; set; }
        public virtual DbSet<VrstaServisa> VrstaServisa { get; set; }
        public virtual DbSet<VrstaUloge> VrstaUloge { get; set; }
        public virtual DbSet<VrstaUredaja> VrstaUredaja { get; set; }
        public virtual DbSet<Zid> Zid { get; set; }

        public RPPP08Context(DbContextOptions<RPPP08Context> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Banka>(entity =>
            {
                entity.HasKey(e => e.Idbanke)
                    .HasName("PK_Banka");

                entity.Property(e => e.Idbanke).HasColumnName("IDBanke");

                entity.Property(e => e.Iban)
                    .IsRequired()
                    .HasColumnName("IBAN")
                    .HasColumnType("char(30)");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Sjediste)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.Swift)
                    .IsRequired()
                    .HasColumnName("SWIFT")
                    .HasColumnType("char(8)");
            });

            modelBuilder.Entity<Format>(entity =>
            {
                entity.HasKey(e => e.Idformata)
                    .HasName("PK_Format");

                entity.Property(e => e.Idformata).HasColumnName("IDFormata");

                entity.Property(e => e.Naziv).HasMaxLength(50);
            });

            modelBuilder.Entity<Komponenta>(entity =>
            {
                entity.HasKey(e => e.Idkomponente)
                    .HasName("PK_Komponenta");

                entity.Property(e => e.Idkomponente).HasColumnName("IDKomponente");

                entity.Property(e => e.IdtipKomponente).HasColumnName("IDTipKomponente");

                entity.Property(e => e.Iduredaja).HasColumnName("IDUredaja");

                entity.HasOne(d => d.IdtipKomponenteNavigation)
                    .WithMany(p => p.Komponenta)
                    .HasForeignKey(d => d.IdtipKomponente)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Komponenta_TipKomponente");

                entity.HasOne(d => d.IduredajaNavigation)
                    .WithMany(p => p.Komponenta)
                    .HasForeignKey(d => d.Iduredaja)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Komponenta_Uredaj");
            });

            modelBuilder.Entity<Osoba>(entity =>
            {
                entity.HasKey(e => e.Idosobe)
                    .HasName("PK_Osoba");

                entity.Property(e => e.Idosobe).HasColumnName("IDOsobe");

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Oib)
                    .IsRequired()
                    .HasColumnName("OIB")
                    .HasMaxLength(50);

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PovijestInstanciPrikazivanja>(entity =>
            {
                entity.HasKey(e => e.IdInstancePrikazivanja)
                    .HasName("PovijestInstanciPrikazivanja_PK");

                entity.Property(e => e.Idsadržaja).HasColumnName("IDSadržaja");

                entity.Property(e => e.Idzida).HasColumnName("IDZida"); 

                entity.Property(e => e.VrijemePrikazivanja).HasColumnType("datetime");

                entity.HasOne(d => d.IdsadržajaNavigation)
                    .WithMany(p => p.PovijestInstanciPrikazivanja)
                    .HasForeignKey(d => d.Idsadržaja)
                   // .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("PovijestInstanciPrikazivanja_Sadržaj_FK");
            });

            modelBuilder.Entity<PovijestPrikazivanja>(entity =>
            {
                entity.HasKey(e => new { e.IdPrograma, e.IdInstancePrikazivanja })
                    .HasName("PovijestPrikazivanja_PK");

                entity.HasOne(d => d.IdInstancePrikazivanjaNavigation)
                    .WithMany(p => p.PovijestPrikazivanja)
                    .HasForeignKey(d => d.IdInstancePrikazivanja)
                    //.OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("PovijestPrikazivanja_PovijestInstanciPrikazivanja_FK");

                entity.HasOne(d => d.IdProgramaNavigation)
                    .WithMany(p => p.PovijestPrikazivanja)
                    .HasForeignKey(d => d.IdPrograma)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("PovijestPrikazivanja_Program_FK");
            });

            modelBuilder.Entity<Program>(entity =>
            {
                entity.HasKey(e => e.IdPrograma)
                    .HasName("Program_PK");

                entity.Property(e => e.ImePrograma)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.IdTipaProgramaNavigation)
                    .WithMany(p => p.Program)
                    .HasForeignKey(d => d.IdTipaPrograma)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Program_TipPrograma");
            });

            modelBuilder.Entity<ProgramAktivan>(entity =>
            {
                entity.HasKey(e => e.IdprogramAktivan)
                    .HasName("PK_ProgramAktivan");

                entity.Property(e => e.IdprogramAktivan).HasColumnName("IDProgramAktivan");

                entity.Property(e => e.Idprograma).HasColumnName("IDPrograma");

                entity.Property(e => e.Idzida).HasColumnName("IDZida");

                entity.Property(e => e.Kraj).HasColumnType("datetime");

                entity.Property(e => e.Pocetak).HasColumnType("datetime");

               // //Janko nadodao 26.04.2017.
               //// entity.HasOne(d => d.IdProgramaNavigation)
               //     .WithMany(p => p.ProgramAktivan)
               //     .HasForeignKey(d => d.Idprograma)
               //     .OnDelete(DeleteBehavior.Restrict)
               //     .HasConstraintName("FK_ProgramAktivan_Program");


                entity.HasOne(d => d.IdzidaNavigation)
                    .WithMany(p => p.ProgramAktivan)
                    .HasForeignKey(d => d.Idzida)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ProgramAktivan_Zid");
            });

            modelBuilder.Entity<SadrzajNaSegmentu>(entity =>
            {
                entity.HasKey(e => e.IdsnaS)
                    .HasName("PK_SadrzajNaSegmentu");

                entity.Property(e => e.IdsnaS).HasColumnName("IDSNaS");

                entity.Property(e => e.Idsadrzaja).HasColumnName("IDSadrzaja");

                entity.Property(e => e.Idsegmenta).HasColumnName("IDSegmenta");

                entity.HasOne(d => d.IdsadrzajaNavigation)
                    .WithMany(p => p.SadrzajNaSegmentu)
                    .HasForeignKey(d => d.Idsadrzaja)
                    //.OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SadrzajNaSegmentu_Sadržaj");

                entity.HasOne(d => d.IdsegmentaNavigation)
                    .WithMany(p => p.SadrzajNaSegmentu)
                    .HasForeignKey(d => d.Idsegmenta)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SadrzajNaSegmentu_Segment");
            });

            modelBuilder.Entity<SadrzajPrograma>(entity =>
            {
                entity.HasKey(e => new { e.IdPrograma, e.IdSadrzaja })
                    .HasName("PK_SadrzajPrograma");

                entity.Property(e => e.IdPrograma).ValueGeneratedOnAdd();

                entity.HasOne(d => d.IdProgramaNavigation)
                    .WithMany(p => p.SadrzajPrograma)
                    .HasForeignKey(d => d.IdPrograma)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("SadrzajPrograma_Program_FK");

                entity.HasOne(d => d.IdSadrzajaNavigation)
                    .WithMany(p => p.SadrzajPrograma)
                    .HasForeignKey(d => d.IdSadrzaja)
                    //.OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SadrzajPrograma_Sadržaj");
            });

            modelBuilder.Entity<Sadržaj>(entity =>
            {
                entity.HasKey(e => e.Idsadržaja)
                    .HasName("PK_Sadržaj");

                entity.Property(e => e.Idsadržaja).HasColumnName("IDSadržaja");

                entity.Property(e => e.Idformata).HasColumnName("IDFormata");

                entity.Property(e => e.IdvrsteSadržaja).HasColumnName("IDVrsteSadržaja");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.IdformataNavigation)
                    .WithMany(p => p.Sadržaj)
                    .HasForeignKey(d => d.Idformata)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Sadržaj_Format");

                entity.HasOne(d => d.IdvrsteSadržajaNavigation)
                    .WithMany(p => p.Sadržaj)
                    .HasForeignKey(d => d.IdvrsteSadržaja)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Sadržaj_VrstaSadržaja");
               
            });

                   

           



            modelBuilder.Entity<Segment>(entity =>
            {
                entity.HasKey(e => e.Idsegmenta)
                    .HasName("PK_Segment");

                entity.Property(e => e.Idsegmenta).HasColumnName("IDSegmenta");

                entity.Property(e => e.Idzida).HasColumnName("IDZida");

                entity.HasOne(d => d.IdzidaNavigation)
                    .WithMany(p => p.Segment)
                    .HasForeignKey(d => d.Idzida)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Segment_Zid");
            });

            modelBuilder.Entity<Servis>(entity =>
            {
                entity.HasKey(e => e.Idservisa)
                    .HasName("PK_Servis");

                entity.Property(e => e.Idservisa).HasColumnName("IDServisa");

                entity.Property(e => e.Idstatusa).HasColumnName("IDStatusa");

                entity.Property(e => e.Iduredaja).HasColumnName("IDUredaja");

                entity.Property(e => e.Idvrste).HasColumnName("IDVrste");

                entity.Property(e => e.Opis).IsRequired();

                entity.Property(e => e.Pocetak).HasColumnType("datetime");

                entity.Property(e => e.Zavrsetak).HasColumnType("datetime");

                entity.HasOne(d => d.IdstatusaNavigation)
                    .WithMany(p => p.Servis)
                    .HasForeignKey(d => d.Idstatusa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Servis_StatusServisa");

                entity.HasOne(d => d.IduredajaNavigation)
                    .WithMany(p => p.Servis)
                    .HasForeignKey(d => d.Iduredaja)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Servis_Uredaj");

                entity.HasOne(d => d.IdvrsteNavigation)
                    .WithMany(p => p.Servis)
                    .HasForeignKey(d => d.Idvrste)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Servis_VrstaServisa");
            });

            modelBuilder.Entity<Serviser>(entity =>
            {
                entity.HasKey(e => e.Idservisera)
                    .HasName("PK_Serviser");

                entity.Property(e => e.Idservisera).HasColumnName("IDServisera");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Idbanke).HasColumnName("IDBanke");

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Oib)
                    .IsRequired()
                    .HasColumnName("OIB")
                    .HasColumnType("char(11)");

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Sjediste)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.Ziroracun)
                    .IsRequired()
                    .HasColumnType("char(30)");

                entity.HasOne(d => d.IdbankeNavigation)
                    .WithMany(p => p.Serviser)
                    .HasForeignKey(d => d.Idbanke)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Serviser_Banka");
            });

            modelBuilder.Entity<StatusServisa>(entity =>
            {
                entity.HasKey(e => e.Idstatusa)
                    .HasName("PK_StatusServisa");

                entity.Property(e => e.Idstatusa).HasColumnName("IDStatusa");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Opis).IsRequired();
            });

            modelBuilder.Entity<TipKomponente>(entity =>
            {
                entity.HasKey(e => e.IdtipKomponente)
                    .HasName("PK_TipKomponente");

                entity.Property(e => e.IdtipKomponente).HasColumnName("IDTipKomponente");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Opis).IsRequired();
            });

            modelBuilder.Entity<TipPrograma>(entity =>
            {
                entity.HasKey(e => e.IdTipaPrograma)
                    .HasName("NewTable_PK");

                entity.Property(e => e.Kontekst).HasColumnType("varchar(100)");

                entity.Property(e => e.NazivTipaPrograma)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<UlogaOsobe>(entity =>
            {
                entity.HasKey(e => e.IdulogeOsobe)
                    .HasName("PK_UlogaOsobe");

                entity.Property(e => e.IdulogeOsobe).HasColumnName("IDUlogeOsobe");

                entity.Property(e => e.Idosobe).HasColumnName("IDOsobe");

                entity.Property(e => e.Idsadržaja).HasColumnName("IDSadržaja");

                entity.Property(e => e.IdvrsteUloge).HasColumnName("IDVrsteUloge");

                entity.HasOne(d => d.IdosobeNavigation)
                    .WithMany(p => p.UlogaOsobe)
                    .HasForeignKey(d => d.Idosobe)
                    .HasConstraintName("FK_UlogaOsobe_Osoba");

                entity.HasOne(d => d.IdsadržajaNavigation)
                    .WithMany(p => p.UlogaOsobe)
                    .HasForeignKey(d => d.Idsadržaja)
                    .HasConstraintName("FK_UlogaOsobe_Sadržaj");

                entity.HasOne(d => d.IdvrsteUlogeNavigation)
                    .WithMany(p => p.UlogaOsobe)
                    .HasForeignKey(d => d.IdvrsteUloge)
                    .HasConstraintName("FK_UlogaOsobe_VrstaUloge");
            });

            modelBuilder.Entity<UlogaServisera>(entity =>
            {
                entity.HasKey(e => new { e.Idservisera, e.Idservisa, e.Uloga })
                    .HasName("PK_UlogaServisera");

                entity.Property(e => e.Idservisera).HasColumnName("IDServisera");

                entity.Property(e => e.Idservisa).HasColumnName("IDServisa");

                entity.Property(e => e.Uloga).HasMaxLength(300);

                entity.Property(e => e.Zapoceo).HasColumnType("datetime");

                entity.Property(e => e.Zavrsio).HasColumnType("datetime");

                entity.HasOne(d => d.IdservisaNavigation)
                    .WithMany(p => p.UlogaServisera)
                    .HasForeignKey(d => d.Idservisa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UlogaServisera_Servis");

                entity.HasOne(d => d.IdserviseraNavigation)
                    .WithMany(p => p.UlogaServisera)
                    .HasForeignKey(d => d.Idservisera)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UlogaServisera_Serviser");
            });

            modelBuilder.Entity<Uredaj>(entity =>
            {
                entity.HasKey(e => e.Iduredaja)
                    .HasName("PK_Uredaj");

                entity.Property(e => e.Iduredaja).HasColumnName("IDUredaja");

                entity.Property(e => e.IdvrsteUredaja).HasColumnName("IDVrsteUredaja");

                entity.Property(e => e.IdzamjenskiUredaj).HasColumnName("IDZamjenskiUredaj");

                entity.Property(e => e.ImeUredaja)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.IdvrsteUredajaNavigation)
                    .WithMany(p => p.Uredaj)
                    .HasForeignKey(d => d.IdvrsteUredaja)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Uredaj_VrstaUredaja");

                entity.HasOne(d => d.IdzamjenskiUredajNavigation)
                    .WithMany(p => p.InverseIdzamjenskiUredajNavigation)
                    .HasForeignKey(d => d.IdzamjenskiUredaj)
                    .HasConstraintName("FK_Uredaj_Uredaj");
            });

            modelBuilder.Entity<UredajZaSegment>(entity =>
            {
                entity.HasKey(e => new { e.Iduredaja, e.Idsegmenta })
                    .HasName("PK_UredajZaSegment");

                entity.Property(e => e.Iduredaja).HasColumnName("IDUredaja");

                entity.Property(e => e.Idsegmenta).HasColumnName("IDSegmenta");

                entity.HasOne(d => d.IdsegmentaNavigation)
                    .WithMany(p => p.UredajZaSegment)
                    .HasForeignKey(d => d.Idsegmenta)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UredajZaSegment_Segment");

                entity.HasOne(d => d.IduredajaNavigation)
                    .WithMany(p => p.UredajZaSegment)
                    .HasForeignKey(d => d.Iduredaja)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UredajZaSegment_Uredaj");
            });

            modelBuilder.Entity<VrstaSadržaja>(entity =>
            {
                entity.HasKey(e => e.IdvrsteSadržaja)
                    .HasName("PK_VrstaSadržaja");

                entity.Property(e => e.IdvrsteSadržaja).HasColumnName("IDVrsteSadržaja");

                entity.Property(e => e.Naziv).HasMaxLength(50);
            });

            modelBuilder.Entity<VrstaServisa>(entity =>
            {
                entity.HasKey(e => e.Idvrste)
                    .HasName("PK_VrstaServisa");

                entity.Property(e => e.Idvrste).HasColumnName("IDVrste");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Opis).IsRequired();
            });

            modelBuilder.Entity<VrstaUloge>(entity =>
            {
                entity.HasKey(e => e.IdvrsteUloge)
                    .HasName("PK_VrstaUloge");

                entity.Property(e => e.IdvrsteUloge).HasColumnName("IDVrsteUloge");

                entity.Property(e => e.Naziv).HasMaxLength(50);
            });

            modelBuilder.Entity<VrstaUredaja>(entity =>
            {
                entity.HasKey(e => e.IdvrsteUredaja)
                    .HasName("PK_VrstaUredaja");

                entity.Property(e => e.IdvrsteUredaja).HasColumnName("IDVrsteUredaja");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Opis).IsRequired();
            });

            modelBuilder.Entity<Zid>(entity =>
            {
                entity.HasKey(e => e.Idzida)
                    .HasName("PK_Zid");

                entity.Property(e => e.Idzida).HasColumnName("IDZida");

                entity.Property(e => e.DatumPostavljanja).HasColumnType("date");

                entity.Property(e => e.Lokacija).IsRequired();
            });
        }
    }
}