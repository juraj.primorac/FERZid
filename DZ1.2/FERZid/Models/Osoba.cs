﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FERZid.Models
{
    public partial class Osoba
    {
        public Osoba()
        {
            UlogaOsobe = new HashSet<UlogaOsobe>();
        }

        public int Idosobe { get; set; }

        [Required]
        public string Ime { get; set; }

        [Required]
        public string Prezime { get; set; }


        [Required]
        [Display(Name = "OIB")]
        [StringLength(11, MinimumLength = 11)]
        public string Oib { get; set; }

        
        public virtual ICollection<UlogaOsobe> UlogaOsobe { get; set; }
    }
}
