﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class Banka
    {
        public Banka()
        {
            Serviser = new HashSet<Serviser>();
        }

        public int Idbanke { get; set; }
        public string Naziv { get; set; }
        public string Iban { get; set; }
        public string Swift { get; set; }
        public string Sjediste { get; set; }

        public virtual ICollection<Serviser> Serviser { get; set; }
    }
}
