﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FERZid.Models
{
    public partial class Format
    {
        public Format()
        {
            Sadržaj = new HashSet<Sadržaj>();
        }

        public int Idformata { get; set; }


        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Naziv { get; set; }

        public virtual ICollection<Sadržaj> Sadržaj { get; set; }
    }
}
