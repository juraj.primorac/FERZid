﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class Komponenta
    {
        public int Idkomponente { get; set; }
        public int IdtipKomponente { get; set; }
        public int Iduredaja { get; set; }

        public virtual TipKomponente IdtipKomponenteNavigation { get; set; }
        public virtual Uredaj IduredajaNavigation { get; set; }
    }
}
