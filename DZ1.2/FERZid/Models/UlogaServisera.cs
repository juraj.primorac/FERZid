﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class UlogaServisera
    {
        public int Idservisera { get; set; }
        public int Idservisa { get; set; }
        public string Uloga { get; set; }
        public DateTime Zapoceo { get; set; }
        public DateTime? Zavrsio { get; set; }

        public virtual Servis IdservisaNavigation { get; set; }
        public virtual Serviser IdserviseraNavigation { get; set; }
    }
}
