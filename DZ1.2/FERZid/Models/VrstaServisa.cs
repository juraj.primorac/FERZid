﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class VrstaServisa
    {
        public VrstaServisa()
        {
            Servis = new HashSet<Servis>();
        }

        public int Idvrste { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }

        public virtual ICollection<Servis> Servis { get; set; }
    }
}
