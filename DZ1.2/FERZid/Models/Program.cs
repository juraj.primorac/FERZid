﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class Program
    {
        public Program()
        {
            PovijestPrikazivanja = new HashSet<PovijestPrikazivanja>();
            SadrzajPrograma = new HashSet<SadrzajPrograma>();
        }

        public int IdPrograma { get; set; }
        public string ImePrograma { get; set; }
        public int IdTipaPrograma { get; set; }
        public int? Trajanje { get; set; }

        //Janko nadodao 26.04.2017.
        public virtual ICollection<ProgramAktivan> ProgramAktivan { get; set; }

        public virtual ICollection<PovijestPrikazivanja> PovijestPrikazivanja { get; set; }
        public virtual ICollection<SadrzajPrograma> SadrzajPrograma { get; set; }
        public virtual TipPrograma IdTipaProgramaNavigation { get; set; }
    }
}
