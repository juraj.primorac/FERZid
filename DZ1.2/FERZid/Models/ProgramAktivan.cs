﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class ProgramAktivan
    {
        public int IdprogramAktivan { get; set; }
        public int Idzida { get; set; }
        public int Idprograma { get; set; }
        public DateTime Pocetak { get; set; }
        public DateTime Kraj { get; set; }

        public virtual Zid IdzidaNavigation { get; set; }
        public virtual Program IdProgramaNavigation { get; set; }
    }
}
