﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class PovijestPrikazivanja
    {
        public int IdPrograma { get; set; }
        public int IdInstancePrikazivanja { get; set; }

        public virtual PovijestInstanciPrikazivanja IdInstancePrikazivanjaNavigation { get; set; }
        public virtual Program IdProgramaNavigation { get; set; }
    }
}
