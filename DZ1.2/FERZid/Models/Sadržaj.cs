﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FERZid.Models
{
    public partial class Sadržaj : IComparable<Sadržaj>
    {
        public Sadržaj()
        {
            PovijestInstanciPrikazivanja = new HashSet<PovijestInstanciPrikazivanja>();
            SadrzajNaSegmentu = new HashSet<SadrzajNaSegmentu>();
            SadrzajPrograma = new HashSet<SadrzajPrograma>();
            UlogaOsobe = new HashSet<UlogaOsobe>();
        }

        public int Idsadržaja { get; set; }


        [Required]        
        [Display(Name = "Vrsta sadržaja")]
        public int IdvrsteSadržaja { get; set; }


        [Required]
        [Display(Name = "Format")]
        public int Idformata { get; set; }


        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Naziv { get; set; }



        public byte[] Podaci { get; set; }



        public long? Veličina { get; set; }
    

        public virtual ICollection<PovijestInstanciPrikazivanja> PovijestInstanciPrikazivanja { get; set; }
        public virtual ICollection<SadrzajNaSegmentu> SadrzajNaSegmentu { get; set; }
        public virtual ICollection<SadrzajPrograma> SadrzajPrograma { get; set; }
        public virtual ICollection<UlogaOsobe> UlogaOsobe { get; set; }
        public virtual Format IdformataNavigation { get; set; }
        public virtual VrstaSadržaja IdvrsteSadržajaNavigation { get; set; }

        public int CompareTo(Sadržaj other)
        {
            if (this.Idsadržaja < other.Idsadržaja) return -1;
            if (this.Idsadržaja > other.Idsadržaja) return 1;
            return 0;
        }

        
    }
}
