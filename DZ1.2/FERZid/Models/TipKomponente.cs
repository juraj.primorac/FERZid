﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class TipKomponente
    {
        public TipKomponente()
        {
            Komponenta = new HashSet<Komponenta>();
        }

        public int IdtipKomponente { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }

        public virtual ICollection<Komponenta> Komponenta { get; set; }
    }
}
