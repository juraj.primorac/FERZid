﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class Uredaj
    {
        public Uredaj()
        {
            Komponenta = new HashSet<Komponenta>();
            Servis = new HashSet<Servis>();
            UredajZaSegment = new HashSet<UredajZaSegment>();
        }

        public int Iduredaja { get; set; }
        public string ImeUredaja { get; set; }
        public int IdvrsteUredaja { get; set; }
        public int? IdzamjenskiUredaj { get; set; }

        public virtual ICollection<Komponenta> Komponenta { get; set; }
        public virtual ICollection<Servis> Servis { get; set; }
        public virtual ICollection<UredajZaSegment> UredajZaSegment { get; set; }
        public virtual VrstaUredaja IdvrsteUredajaNavigation { get; set; }
        public virtual Uredaj IdzamjenskiUredajNavigation { get; set; }
        public virtual ICollection<Uredaj> InverseIdzamjenskiUredajNavigation { get; set; }
    }
}
