﻿using System;
using System.Collections.Generic;

namespace FERZid.Models
{
    public partial class Segment
    {
        public Segment()
        {
            SadrzajNaSegmentu = new HashSet<SadrzajNaSegmentu>();
            UredajZaSegment = new HashSet<UredajZaSegment>();
        }

        public int Idsegmenta { get; set; }
        public int Idzida { get; set; }
        public int KoordinataX { get; set; }
        public int KoordinataY { get; set; }
        public int Visina { get; set; }
        public int Sirina { get; set; }

        public virtual ICollection<SadrzajNaSegmentu> SadrzajNaSegmentu { get; set; }
        public virtual ICollection<UredajZaSegment> UredajZaSegment { get; set; }
        public virtual Zid IdzidaNavigation { get; set; }
    }
}
