﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FERZid.Models
{
    public partial class VrstaSadržaja
    {
        public VrstaSadržaja()
        {
            Sadržaj = new HashSet<Sadržaj>();
        }

        public int IdvrsteSadržaja { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Naziv { get; set; }

        public string Opis { get; set; }



        public virtual ICollection<Sadržaj> Sadržaj { get; set; }

        
    }
}
