using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class SadrzajProgramaController : Controller
    {
        private readonly RPPP08Context _context;

        public SadrzajProgramaController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: SadrzajPrograma
        public async Task<IActionResult> Index()
        {
            var rPPP08Context = _context.SadrzajPrograma.Include(s => s.IdProgramaNavigation).Include(s => s.IdSadrzajaNavigation);
            return View(await rPPP08Context.ToListAsync());
        }

        // GET: SadrzajPrograma/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sadrzajPrograma = await _context.SadrzajPrograma
                .Include(s => s.IdProgramaNavigation)
                .Include(s => s.IdSadrzajaNavigation)
                .SingleOrDefaultAsync(m => m.IdPrograma == id);
            if (sadrzajPrograma == null)
            {
                return NotFound();
            }

            return View(sadrzajPrograma);
        }

        // GET: SadrzajPrograma/Create
        public IActionResult Create()
        {
            ViewData["IdPrograma"] = new SelectList(_context.Program, "IdPrograma", "ImePrograma");
            ViewData["IdSadrzaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv");
            return View();
        }

        // POST: SadrzajPrograma/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdPrograma,IdSadrzaja")] SadrzajPrograma sadrzajPrograma)
        {
            if (ModelState.IsValid)
            {
                _context.Add(sadrzajPrograma);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdPrograma"] = new SelectList(_context.Program, "IdPrograma", "ImePrograma", sadrzajPrograma.IdPrograma);
            ViewData["IdSadrzaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", sadrzajPrograma.IdSadrzaja);
            return View(sadrzajPrograma);
        }

        // GET: SadrzajPrograma/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sadrzajPrograma = await _context.SadrzajPrograma.SingleOrDefaultAsync(m => m.IdPrograma == id);
            if (sadrzajPrograma == null)
            {
                return NotFound();
            }
            ViewData["IdPrograma"] = new SelectList(_context.Program, "IdPrograma", "ImePrograma", sadrzajPrograma.IdPrograma);
            ViewData["IdSadrzaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", sadrzajPrograma.IdSadrzaja);
            return View(sadrzajPrograma);
        }

        // POST: SadrzajPrograma/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdPrograma,IdSadrzaja")] SadrzajPrograma sadrzajPrograma)
        {
            if (id != sadrzajPrograma.IdPrograma)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(sadrzajPrograma);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SadrzajProgramaExists(sadrzajPrograma.IdPrograma))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdPrograma"] = new SelectList(_context.Program, "IdPrograma", "ImePrograma", sadrzajPrograma.IdPrograma);
            ViewData["IdSadrzaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", sadrzajPrograma.IdSadrzaja);
            return View(sadrzajPrograma);
        }

        // GET: SadrzajPrograma/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sadrzajPrograma = await _context.SadrzajPrograma
                .Include(s => s.IdProgramaNavigation)
                .Include(s => s.IdSadrzajaNavigation)
                .SingleOrDefaultAsync(m => m.IdPrograma == id);
            if (sadrzajPrograma == null)
            {
                return NotFound();
            }

            return View(sadrzajPrograma);
        }

        // POST: SadrzajPrograma/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sadrzajPrograma = await _context.SadrzajPrograma.SingleOrDefaultAsync(m => m.IdPrograma == id);
            _context.SadrzajPrograma.Remove(sadrzajPrograma);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool SadrzajProgramaExists(int id)
        {
            return _context.SadrzajPrograma.Any(e => e.IdPrograma == id);
        }
    }
}
