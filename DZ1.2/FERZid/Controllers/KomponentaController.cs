using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class KomponentaController : Controller
    {
        private readonly RPPP08Context _context;

        public KomponentaController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: Komponenta
        public async Task<IActionResult> Index(string sortOrder,string searchString, string currentFilter, int? page)
        {
            ViewData["TypeSortParm"] = String.IsNullOrEmpty(sortOrder) ? "type_desc" : "";
            ViewData["CurrentFilter"] = searchString;
            var komponente = _context.Komponenta.Include(k => k.IdtipKomponenteNavigation).Include(k => k.IduredajaNavigation).AsQueryable();
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                komponente = komponente.Where(k => k.IdtipKomponenteNavigation.Naziv.Contains(searchString));
            }

            switch (sortOrder){
                case "type_desc":
                    komponente = komponente.OrderByDescending(k => k.IdtipKomponenteNavigation.Naziv);
                    break;
                default:
                    komponente = komponente.OrderBy(k => k.IdtipKomponenteNavigation.Naziv);
                    break;
            }

            int pageSize = 20;
            return View(await PaginatedList<Komponenta>.CreateAsync(komponente.AsNoTracking(), page ?? 1, pageSize));

        }

        // GET: Komponenta/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var komponenta = await _context.Komponenta
                .Include(k => k.IdtipKomponenteNavigation)
                .Include(k => k.IduredajaNavigation)
                .SingleOrDefaultAsync(m => m.Idkomponente == id);
            if (komponenta == null)
            {
                return NotFound();
            }

            return View(komponenta);
        }

        // GET: Komponenta/Create
        public IActionResult Create()
        {
            ViewData["IdtipKomponente"] = new SelectList(_context.TipKomponente, "IdtipKomponente", "Naziv");
            ViewData["Iduredaja"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja");
            return View();
        }

        // POST: Komponenta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idkomponente,IdtipKomponente,Iduredaja")] Komponenta komponenta)
        {
            if (ModelState.IsValid)
            {
                _context.Add(komponenta);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdtipKomponente"] = new SelectList(_context.TipKomponente, "IdtipKomponente", "Naziv", komponenta.IdtipKomponente);
            ViewData["Iduredaja"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja", komponenta.Iduredaja);
            return View(komponenta);
        }

        // GET: Komponenta/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var komponenta = await _context.Komponenta.SingleOrDefaultAsync(m => m.Idkomponente == id);
            if (komponenta == null)
            {
                return NotFound();
            }
            ViewData["IdtipKomponente"] = new SelectList(_context.TipKomponente, "IdtipKomponente", "Naziv", komponenta.IdtipKomponente);
            ViewData["Iduredaja"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja", komponenta.Iduredaja);
            return View(komponenta);
        }

        // POST: Komponenta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idkomponente,IdtipKomponente,Iduredaja")] Komponenta komponenta)
        {
            if (id != komponenta.Idkomponente)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(komponenta);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KomponentaExists(komponenta.Idkomponente))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdtipKomponente"] = new SelectList(_context.TipKomponente, "IdtipKomponente", "Naziv", komponenta.IdtipKomponente);
            ViewData["Iduredaja"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja", komponenta.Iduredaja);
            return View(komponenta);
        }

        // GET: Komponenta/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var komponenta = await _context.Komponenta
                .Include(k => k.IdtipKomponenteNavigation)
                .Include(k => k.IduredajaNavigation)
                .SingleOrDefaultAsync(m => m.Idkomponente == id);
            if (komponenta == null)
            {
                return NotFound();
            }

            return View(komponenta);
        }

        // POST: Komponenta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var komponenta = await _context.Komponenta.SingleOrDefaultAsync(m => m.Idkomponente == id);
            _context.Komponenta.Remove(komponenta);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool KomponentaExists(int id)
        {
            return _context.Komponenta.Any(e => e.Idkomponente == id);
        }
    }
}
