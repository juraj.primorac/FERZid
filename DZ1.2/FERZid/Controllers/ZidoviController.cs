using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class ZidoviController : Controller
    {
        private readonly RPPP08Context _context;

        public ZidoviController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: Zidovi
        public async Task<IActionResult> Index()
        {
            //var rPPP08Context = _context.Segment.Include(s => s.IdzidaNavigation);
            

            var zidovi = await _context.Zid.Include(s => s.ProgramAktivan)
                .Include(s => s.Segment)
                .ToListAsync();

            //foreach (var el in zidovi)
            //{
            //    foreach(var progakt in el.ProgramAktivan)
            //    {
            //        Console.Write(progakt.IdProgramaNavigation.ImePrograma+" ");
            //    }
            //    Console.WriteLine();
            //}

                return View(zidovi);
        }

        // GET: Zidovi/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var zid = await _context.Zid
                .SingleOrDefaultAsync(m => m.Idzida == id);
            if (zid == null)
            {
                return NotFound();
            }

            return View(zid);
        }

        // GET: Zidovi/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Zidovi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idzida,DatumPostavljanja,Lokacija,Opis,Visina,Sirina")] Zid zid)
        {
            if (ModelState.IsValid)
            {
                _context.Add(zid);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(zid);
        }

        // GET: Zidovi/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var zid = await _context.Zid.SingleOrDefaultAsync(m => m.Idzida == id);
            if (zid == null)
            {
                return NotFound();
            }
            return View(zid);
        }

        // POST: Zidovi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idzida,DatumPostavljanja,Lokacija,Opis,Visina,Sirina")] Zid zid)
        {
            if (id != zid.Idzida)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(zid);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ZidExists(zid.Idzida))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(zid);
        }

        // GET: Zidovi/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var zid = await _context.Zid
                .SingleOrDefaultAsync(m => m.Idzida == id);
            if (zid == null)
            {
                return NotFound();
            }

            return View(zid);
        }

        // POST: Zidovi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var zid = await _context.Zid.SingleOrDefaultAsync(m => m.Idzida == id);
            _context.Zid.Remove(zid);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool ZidExists(int id)
        {
            return _context.Zid.Any(e => e.Idzida == id);
        }
    }
}
