using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class SegmentController : Controller
    {
        private readonly RPPP08Context _context;

        public SegmentController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: Segment
        public async Task<IActionResult> Index()
        {
            var rPPP08Context = _context.Segment.Include(s => s.IdzidaNavigation);
            return View(await rPPP08Context.ToListAsync());
        }

        // GET: Segment/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var segment = await _context.Segment
                .Include(s => s.IdzidaNavigation)
                .SingleOrDefaultAsync(m => m.Idsegmenta == id);
            if (segment == null)
            {
                return NotFound();
            }

            return View(segment);
        }

        // GET: Segment/Create
        public IActionResult Create()
        {
            ViewData["Idzida"] = new SelectList(_context.Zid, "Idzida", "Lokacija");
            return View();
        }

        // POST: Segment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idsegmenta,Idzida,KoordinataX,KoordinataY,Visina,Sirina")] Segment segment)
        {
            if (ModelState.IsValid)
            {
                _context.Add(segment);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["Idzida"] = new SelectList(_context.Zid, "Idzida", "Lokacija", segment.Idzida);
            return View(segment);
        }

        // GET: Segment/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var segment = await _context.Segment.SingleOrDefaultAsync(m => m.Idsegmenta == id);
            if (segment == null)
            {
                return NotFound();
            }
            ViewData["Idzida"] = new SelectList(_context.Zid, "Idzida", "Lokacija", segment.Idzida);
            return View(segment);
        }

        // POST: Segment/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idsegmenta,Idzida,KoordinataX,KoordinataY,Visina,Sirina")] Segment segment)
        {
            if (id != segment.Idsegmenta)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(segment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SegmentExists(segment.Idsegmenta))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["Idzida"] = new SelectList(_context.Zid, "Idzida", "Lokacija", segment.Idzida);
            return View(segment);
        }

        // GET: Segment/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var segment = await _context.Segment
                .Include(s => s.IdzidaNavigation)
                .SingleOrDefaultAsync(m => m.Idsegmenta == id);
            if (segment == null)
            {
                return NotFound();
            }

            return View(segment);
        }

        // POST: Segment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var segment = await _context.Segment.SingleOrDefaultAsync(m => m.Idsegmenta == id);
            _context.Segment.Remove(segment);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool SegmentExists(int id)
        {
            return _context.Segment.Any(e => e.Idsegmenta == id);
        }
    }
}
