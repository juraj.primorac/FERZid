using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class OsobaController : Controller
    {
        private readonly RPPP08Context _context;

        public OsobaController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: Osoba
        public async Task<IActionResult> Index(string ime,string prezime,string oib,string sortOrder,string currentIme,string currentPrezime,string currentOib, string errorMessage, string successMessage, int page=0)
        {


            ViewBag.CurrentSort = sortOrder;

            ViewBag.imeSortParam = String.IsNullOrEmpty(sortOrder) ? "ime_desc" : "";
            ViewBag.prezimeSortParam = sortOrder == "prezime_desc" ? "prezime_asc" : "prezime_desc";
            ViewBag.oibSortParam = sortOrder == "oib_desc" ? "oib_asc" : "oib_desc";

            if (ime != null || prezime != null || oib != null)
            {
                page = 0;
            }

            if (ime == null) ime = currentIme;
            if (prezime == null) prezime = currentPrezime;
            if (oib == null) oib = currentOib;

            ViewBag.CurrentIme = ime;
            ViewBag.CurrentPrezime = prezime;
            ViewBag.CurrentOib = oib;

            var osob = from m in _context.Osoba
                       select m;


            if (!String.IsNullOrEmpty(ime))
            {
                osob = osob.Where(s => s.Ime.Contains(ime));
            }

            if (!String.IsNullOrEmpty(prezime))
            {
                osob = osob.Where(s => s.Prezime.Contains(prezime));
            }

            if (!String.IsNullOrEmpty(oib))
            {
                osob = osob.Where(s => s.Oib.Contains(oib));
            }



            switch (sortOrder)
            {

                case "ime_desc":
                    osob = osob.OrderByDescending(s => s.Ime);
                    break;
                case "prezime_asc":
                    osob = osob.OrderBy(s => s.Prezime);
                    break;
                case "prezime_desc":
                    osob = osob.OrderByDescending(s => s.Prezime);
                    break;
                case "oib_asc":
                    osob = osob.OrderBy(s => s.Oib);
                    break;
                case "oib_desc":
                    osob = osob.OrderByDescending(s => s.Oib);
                    break;
                default:
                    osob = osob.OrderBy(s => s.Ime);
                    break;
            }

            const int pageSize = 3;
            var count = osob.Count();
            if (count == 0) page = 0;
            else if ((page * pageSize) >= count) --page;
            osob = osob.Skip(page * pageSize).Take(pageSize);
            this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
            this.ViewBag.Page = page;


            var osobaViewModel = new OsobaViewModel();
            osobaViewModel.osobe = await osob.ToListAsync();

            ViewData["Error"] = errorMessage;
            TempData["Success"] = successMessage;

            return View(osobaViewModel);
        }

        // GET: Osoba/Details/5
        public async Task<IActionResult> Details(int? id, int? previousDetail, int? nextDetail, string sortOrder, string currentIme, string currentPrezime, string currentOib, int page)
        {
            if (id == null && previousDetail == null && nextDetail == null)
            {
                return NotFound();
            }

            var osob = from m in _context.Osoba
                select m;

            List<Osoba> osobe = await osob.ToListAsync();

            


            if (previousDetail != null)
            {
                id = osobe.ElementAt((int)previousDetail).Idosobe;
            }

            if (nextDetail != null)
            {
                id = osobe.ElementAt((int)nextDetail).Idosobe;
            }



            var osoba = await _context.Osoba
               .AsNoTracking()
               .Where(m => m.Idosobe == id)
               .Select(s => new OsobaViewModel
               {
                    Idosobe= s.Idosobe,
                    Ime =s.Ime,
                    Prezime=s.Prezime,
                    Oib =s.Oib
                })
               .FirstOrDefaultAsync();



            if (osoba == null)
            {
                return NotFound();
            }
            else {

                osoba.osobe = osobe;
                int indexAktualni = osoba.osobe.FindIndex(k => k.Idosobe== id);
                ViewBag.IndexAktualni = indexAktualni;
                ViewBag.NumberOfElements = osoba.osobe.Count();

            }

            ViewBag.Page = page;
            ViewBag.CurrentIme = currentIme;
            ViewBag.CurrentPrezime = currentPrezime;
            ViewBag.CurrentOib = currentOib;
            ViewBag.CurrentSort = sortOrder;

            return View(osoba);
        }

        // GET: Osoba/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Osoba/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idosobe,Ime,Prezime,Oib")] Osoba osoba)
        {
            string successMessage = null;

            if (ModelState.IsValid)
            {
                _context.Add(osoba);
                await _context.SaveChangesAsync();

                successMessage = "Osoba created: " + osoba.Ime+" "+osoba.Prezime;
                return RedirectToAction("Index", new { successMessage = successMessage });
            }
            return View(osoba);
        }

        // GET: Osoba/Edit/5
        public async Task<IActionResult> Edit(int? id, string sortOrder, string currentIme, string currentPrezime, string currentOib, int page, string successMessage)
        {
            if (id == null)
            {
                return NotFound();
            }

            var osoba = await _context.Osoba.SingleOrDefaultAsync(m => m.Idosobe == id);
            if (osoba == null)
            {
                return NotFound();
            }

            ViewBag.Page = page;
            ViewBag.CurrentIme = currentIme;
            ViewBag.CurrentPrezime = currentPrezime;
            ViewBag.CurrentOib = currentOib;
            ViewBag.CurrentSort = sortOrder;

            if (successMessage != null) TempData["Success"] = successMessage;
            return View(osoba);
        }

        // POST: Osoba/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, string sortOrder, string currentIme, string currentPrezime, string currentOib, int page, [Bind("Idosobe,Ime,Prezime,Oib")] Osoba osoba)
        {
            string successMessage = null;

            if (id != osoba.Idosobe)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(osoba);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OsobaExists(osoba.Idosobe))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                successMessage = "Osoba edited: " + osoba.Ime + " " + osoba.Prezime;
                return RedirectToAction("Edit", new { page = page, sortOrder = sortOrder, currentIme = currentIme, currentPrezime = currentPrezime, currentOib = currentOib, successMessage = successMessage });
            }
            return View(osoba);
        }

        // GET: Osoba/Delete/5
        public async Task<IActionResult> Delete(int? id, string sortOrder, string currentIme, string currentPrezime, string currentOib, int page)
        {
            if (id == null)
            {
                return NotFound();
            }

            var osoba = await _context.Osoba
                .SingleOrDefaultAsync(m => m.Idosobe == id);
            if (osoba == null)
            {
                return NotFound();
            }

            //return View(osoba);
            return await DeleteConfirmed((int)id,sortOrder,currentIme,currentPrezime,currentOib,page);
        }

        // POST: Osoba/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, string sortOrder, string currentIme, string currentPrezime, string currentOib, int page)
        {
            string errorMessage = null;
            string successMessage = null;

            var osoba = await _context.Osoba.SingleOrDefaultAsync(m => m.Idosobe == id);
            _context.Osoba.Remove(osoba);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                errorMessage = "Osoba delete failed: " + osoba.Ime+" "+osoba.Prezime;
            }
            if (errorMessage == null) successMessage = "Osoba deleted: " + osoba.Ime + " " + osoba.Prezime;
            return RedirectToAction("Index", new { page = page, sortOrder = sortOrder, currentIme = currentIme, currentPrezime = currentPrezime, currentOib = currentOib, errorMessage = errorMessage, successMessage = successMessage });
        }

        private bool OsobaExists(int id)
        {
            return _context.Osoba.Any(e => e.Idosobe == id);
        }
    }
}
