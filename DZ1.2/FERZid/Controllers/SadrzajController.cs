using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;
//using PagedList;

namespace FERZid.Controllers
{
    public class SadrzajController : Controller
    {
        private readonly RPPP08Context _context;

        public SadrzajController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: Sadrzaj
        public async Task<IActionResult> Index(string searchString, string formatSadrzaja, string vrstaSadrzaja, string sortOrder, string currentFilter, string currentFormat, string currentVrsta, string errorMessage, string successMessage, int page=0)
        {
           
         


            IQueryable<string> formatQuery = from m in _context.Sadržaj
                                             .Include(s => s.IdformataNavigation)
                                             .Include(s => s.IdvrsteSadržajaNavigation)
                                             orderby m.IdformataNavigation.Naziv
                                             select m.IdformataNavigation.Naziv;

            IQueryable<string> vrstaSadrzajaQuery = from m in _context.Sadržaj
                                             .Include(s => s.IdformataNavigation)
                                             .Include(s => s.IdvrsteSadržajaNavigation)
                                                    orderby m.IdvrsteSadržajaNavigation.Naziv
                                                    select m.IdvrsteSadržajaNavigation.Naziv;

            ViewBag.CurrentSort = sortOrder;

            ViewBag.nazivSortParam = String.IsNullOrEmpty(sortOrder) ? "naziv_desc" : "";
            ViewBag.formatSortParam = sortOrder == "format_desc" ? "format_asc" : "format_desc";
            ViewBag.vrstaSadrzajaSortParam = sortOrder == "vrstaSadrzaja_desc" ? "vrstaSadrzaja_asc" : "vrstaSadrzaja_desc";



            if (searchString != null || formatSadrzaja !=null || vrstaSadrzaja != null)
            {
                page = 0;
            }
            
            if (searchString == null) searchString = currentFilter;
            if (formatSadrzaja == null) formatSadrzaja = currentFormat;
            if (vrstaSadrzaja == null) vrstaSadrzaja = currentVrsta;
            



            ViewBag.CurrentFilter = searchString;
            ViewBag.CurrentFormat = formatSadrzaja;
            ViewBag.CurrentVrsta = vrstaSadrzaja;

            var sadr = from m in _context.Sadržaj
                           .Include(s => s.IdformataNavigation)
                           .Include(s => s.IdvrsteSadržajaNavigation)
                       select m;   


            if (!String.IsNullOrEmpty(searchString))
            {
                sadr = sadr.Where(s => s.Naziv.Contains(searchString));
            }
            
            if (!String.IsNullOrEmpty(formatSadrzaja))
            {
                sadr = sadr.Where(x => x.IdformataNavigation.Naziv == formatSadrzaja);
            }

            if (!String.IsNullOrEmpty(vrstaSadrzaja))
            {
                sadr = sadr.Where(y => y.IdvrsteSadržajaNavigation.Naziv == vrstaSadrzaja);
            }
            

            switch (sortOrder)
            {

                case "naziv_desc":
                    sadr = sadr.OrderByDescending(s => s.Naziv);
                    break;
                case "format_asc":
                    sadr = sadr.OrderBy(s => s.IdformataNavigation.Naziv);
                    break;
                case "format_desc":
                    sadr = sadr.OrderByDescending(s => s.IdformataNavigation.Naziv);
                    break;
                case "vrstaSadrzaja_asc":
                    sadr = sadr.OrderBy(s => s.IdvrsteSadržajaNavigation.Naziv);
                    break;
                case "vrstaSadrzaja_desc":
                    sadr = sadr.OrderByDescending(s => s.IdvrsteSadržajaNavigation.Naziv);
                    break;
                default:
                    sadr = sadr.OrderBy(s => s.Naziv);
                    break;
            }

            const int pageSize = 3;
            var count = sadr.Count();
            if (count == 0) page = 0;
            else if ((page * pageSize) >= count) --page;

            sadr = sadr.Skip(page * pageSize).Take(pageSize);
            this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
            this.ViewBag.Page = page;

            var sadrzajViewModel = new SadrzajViewModel();
            
            sadrzajViewModel.formati= new SelectList(await formatQuery.Distinct().ToListAsync());
            sadrzajViewModel.vrsteSadrzaja = new SelectList(await vrstaSadrzajaQuery.Distinct().ToListAsync());
            sadrzajViewModel.sadrzaji = await sadr.ToListAsync();
            sadrzajViewModel.sadrzaji.ForEach( m =>
            {
               var uloge = _context.UlogaOsobe
                    .Where(s => s.Idsadržaja == m.Idsadržaja)
                    .Select(s => new UlogaOsobeViewModel
                    {
                        IdulogeOsobe = s.IdulogeOsobe,
                        IdvrsteUloge = s.IdvrsteUloge,
                        NazivVrsteUloge = s.IdvrsteUlogeNavigation.Naziv,
                        Idosobe = s.Idosobe,
                        ImeIPrezimeOsobe = s.IdosobeNavigation.Ime + " " + s.IdosobeNavigation.Prezime,
                        Idsadržaja = s.Idsadržaja,
                        NazivSadrzaja = s.IdsadržajaNavigation.Naziv
                    })
                    .ToList();
                m.ulogeNadSadrzajem = uloge;
                
            });


            ViewData["Error"] = errorMessage;
            TempData["Success"] = successMessage;

            return View(sadrzajViewModel);

        }

        // GET: Sadrzaj/Details/5
        public async Task<IActionResult> Details(int? id, int? previousDetail, int? nextDetail, string sortOrder, string currentFilter, string currentFormat, string currentVrsta, int page)
        {
            if (id == null && previousDetail == null && nextDetail == null)
            {
                return NotFound();
            }

            var sadr = from m in _context.Sadržaj
                           .Include(s => s.IdformataNavigation)
                           .Include(s => s.IdvrsteSadržajaNavigation)
                       select m;
            List<Sadržaj> sadrzaji = await sadr.ToListAsync();

           

            if (previousDetail != null)
            {
                id = sadrzaji.ElementAt((int)previousDetail).Idsadržaja;
            }

            if (nextDetail != null)
            {
                id = sadrzaji.ElementAt((int)nextDetail).Idsadržaja;
            }

            var sadrzaj = await _context.Sadržaj
               .AsNoTracking()
               .Where(m => m.Idsadržaja == id)
               .Select(s => new SadrzajViewModel
               {
                   Idsadržaja = s.Idsadržaja,
                   Nazivsadržaja = s.Naziv,
                   IdvrsteSadržaja =s.IdvrsteSadržaja,
                   NazivVrsteSadržaja =s.IdvrsteSadržajaNavigation.Naziv,
                   Idformata = s.Idformata,
                   NazivFormata = s.IdformataNavigation.Naziv,
                   Podaci = s.Podaci,
                   Veličina = s.Veličina
                })
               .FirstOrDefaultAsync();

            if (sadrzaj == null)
            {
                return NotFound();
            }
            else
            {
                
                sadrzaj.sadrzaji = sadrzaji;
                int indexAktualni = sadrzaj.sadrzaji.FindIndex(k => k.Idsadržaja == id);
                ViewBag.IndexAktualni = indexAktualni;
                ViewBag.NumberOfElements = sadrzaj.sadrzaji.Count();
                
                


                var uloge = await _context.UlogaOsobe
                    .Where(s => s.Idsadržaja == sadrzaj.Idsadržaja)
                    .Select(s => new UlogaOsobeViewModel
                    {
                        IdulogeOsobe = s.IdulogeOsobe,
                        IdvrsteUloge = s.IdvrsteUloge,
                        NazivVrsteUloge = s.IdvrsteUlogeNavigation.Naziv,
                        Idosobe = s.Idosobe,
                        ImeIPrezimeOsobe = s.IdosobeNavigation.Ime + " " + s.IdosobeNavigation.Prezime,
                        Idsadržaja = s.Idsadržaja,
                        NazivSadrzaja = s.IdsadržajaNavigation.Naziv
                    })
                    .ToListAsync();

                sadrzaj.Uloge = uloge;
            }

            ViewBag.Page = page;
            ViewBag.CurrentFilter = currentFilter;
            ViewBag.CurrentFormat = currentFormat;
            ViewBag.CurrentVrsta = currentVrsta;
            ViewBag.CurrentSort = sortOrder;

            return View(sadrzaj);
        }

        // GET: Sadrzaj/Create
        public IActionResult Create()
        {
            ViewData["Idformata"] = new SelectList(_context.Format, "Idformata", "Naziv");
            ViewData["IdvrsteSadržaja"] = new SelectList(_context.VrstaSadržaja, "IdvrsteSadržaja", "Naziv");
            return View();
        }

        // POST: Sadrzaj/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idsadržaja,IdvrsteSadržaja,Idformata,Naziv,Podaci,Veličina")] Sadržaj sadržaj)
        {
            string successMessage = null;

            if (ModelState.IsValid)
            {
                _context.Add(sadržaj);
                await _context.SaveChangesAsync();
                successMessage = "Sadržaj created: " + sadržaj.Naziv;
                return RedirectToAction("Index", new { successMessage = successMessage });
            }
            ViewData["Idformata"] = new SelectList(_context.Format, "Idformata", "Naziv", sadržaj.Idformata);
            ViewData["IdvrsteSadržaja"] = new SelectList(_context.VrstaSadržaja, "IdvrsteSadržaja", "Naziv", sadržaj.IdvrsteSadržaja);
            return View(sadržaj);
        }

        // GET: Sadrzaj/Edit/5
        public async Task<IActionResult> Edit(int? id, string sortOrder, string currentFilter, string currentFormat, string currentVrsta, int page, string successMessage)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sadržaj = await _context.Sadržaj.SingleOrDefaultAsync(m => m.Idsadržaja == id);
            if (sadržaj == null)
            {
                return NotFound();
            }
            ViewData["Idformata"] = new SelectList(_context.Format, "Idformata", "Naziv", sadržaj.Idformata);
            ViewData["IdvrsteSadržaja"] = new SelectList(_context.VrstaSadržaja, "IdvrsteSadržaja", "Naziv", sadržaj.IdvrsteSadržaja);

            ViewBag.Page = page;
            ViewBag.CurrentFilter = currentFilter;
            ViewBag.CurrentFormat = currentFormat;
            ViewBag.CurrentVrsta = currentVrsta;
            ViewBag.CurrentSort = sortOrder;

            if (successMessage != null) TempData["Success"] = successMessage;
            return View(sadržaj);
        }

        // POST: Sadrzaj/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,string sortOrder, string currentFilter, string currentFormat, string currentVrsta, int page, [Bind("Idsadržaja,IdvrsteSadržaja,Idformata,Naziv,Podaci,Veličina")] Sadržaj sadržaj)
        {
            string successMessage = null;

            if (id != sadržaj.Idsadržaja)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(sadržaj);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SadržajExists(sadržaj.Idsadržaja))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                successMessage = "Sadržaj edited: " + sadržaj.Naziv;
                return RedirectToAction("Edit",new { page=page, sortOrder = sortOrder , currentFilter = currentFilter, currentVrsta = currentVrsta, currentFormat=currentFormat, successMessage = successMessage });
            }
            ViewData["Idformata"] = new SelectList(_context.Format, "Idformata", "Naziv", sadržaj.Idformata);
            ViewData["IdvrsteSadržaja"] = new SelectList(_context.VrstaSadržaja, "IdvrsteSadržaja", "Naziv", sadržaj.IdvrsteSadržaja);
            return View(sadržaj);
        }

        // GET: Sadrzaj/Delete/5
        public async Task<IActionResult> Delete(int? id, string sortOrder, string currentFilter, string currentFormat, string currentVrsta, int page)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sadržaj = await _context.Sadržaj
                .Include(s => s.IdformataNavigation)
                .Include(s => s.IdvrsteSadržajaNavigation)
                .SingleOrDefaultAsync(m => m.Idsadržaja == id);
            if (sadržaj == null)
            {
                return NotFound();
            }

            //return View(sadržaj);
            return await DeleteConfirmed((int)id, sortOrder,  currentFilter,  currentFormat,  currentVrsta,  page);
        }

        // POST: Sadrzaj/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, string sortOrder, string currentFilter, string currentFormat, string currentVrsta, int page)
        {
            string errorMessage = null;
            string successMessage = null;

            var sadržaj = await _context.Sadržaj.SingleOrDefaultAsync(m => m.Idsadržaja == id);
            //var ulogaOsobe= await _context.UlogaOsobe.SingleOrDefaultAsync(m => m.Idsadržaja == id);
            //var povijestInstanciPrikazivanja = await _context.PovijestInstanciPrikazivanja.SingleOrDefaultAsync(m => m.Idsadržaja == id);
            //if (!(povijestInstanciPrikazivanja == null))
            //{
            //    var povijestPrikazivanja = await _context.PovijestPrikazivanja.SingleOrDefaultAsync(m => m.IdInstancePrikazivanja == povijestInstanciPrikazivanja.IdInstancePrikazivanja);
            //    if (!(povijestPrikazivanja == null)) _context.PovijestPrikazivanja.Remove(povijestPrikazivanja);
            //    _context.PovijestInstanciPrikazivanja.Remove(povijestInstanciPrikazivanja);
            //}
            //var sadrzajNaSegmentu= await _context.SadrzajNaSegmentu.SingleOrDefaultAsync(m => m.Idsadrzaja == id);
            //var sadrzajPrograma= await _context.SadrzajPrograma.SingleOrDefaultAsync(m => m.IdSadrzaja == id);
            //if (!(ulogaOsobe == null)) _context.UlogaOsobe.Remove(ulogaOsobe);
            //if (!(sadrzajNaSegmentu == null)) _context.SadrzajNaSegmentu.Remove(sadrzajNaSegmentu);
            //if (!(sadrzajPrograma == null)) _context.SadrzajPrograma.Remove(sadrzajPrograma);
            _context.Sadržaj.Remove(sadržaj);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                errorMessage = "Sadržaj delete failed:  " + sadržaj.Naziv;
            }

            if (errorMessage == null) successMessage = "Format deleted: " + sadržaj.Naziv;
            return RedirectToAction("Index", new { page = page, sortOrder = sortOrder, currentFilter = currentFilter, currentVrsta = currentVrsta, currentFormat= currentFormat, errorMessage = errorMessage, successMessage = successMessage });
        }

        private bool SadržajExists(int id)
        {
            return _context.Sadržaj.Any(e => e.Idsadržaja == id);
        }
    }
}
