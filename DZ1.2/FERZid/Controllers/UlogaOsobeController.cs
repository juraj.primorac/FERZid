using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class UlogaOsobeController : Controller
    {
        private readonly RPPP08Context _context;

        public UlogaOsobeController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: UlogaOsobe
        public async Task<IActionResult> Index(string currentVrstaUloge,string currentSadrzaj,string currentOsoba,string osoba,string sadrzaj,string vrstaUloge,string sortOrder,int page=0)
        {
           


            IQueryable<string> osobaQuery = from m in _context.UlogaOsobe
                                            .Include(x => x.IdosobeNavigation)
                                            .Include(x => x.IdsadržajaNavigation)
                                            .Include(x => x.IdvrsteUlogeNavigation)
                                             orderby m.IdosobeNavigation.Prezime
                                             orderby m.IdosobeNavigation.Ime
                                             select m.IdosobeNavigation.Ime +" "+ m.IdosobeNavigation.Prezime;

            IQueryable<string> sadrzajQuery = from m in _context.UlogaOsobe
                                             .Include(x => x.IdosobeNavigation)
                                             .Include(x => x.IdsadržajaNavigation)
                                             .Include(x => x.IdvrsteUlogeNavigation)
                                              orderby m.IdsadržajaNavigation.Naziv
                                              select m.IdsadržajaNavigation.Naziv;

            IQueryable<string> vrstaUlogeQuery = from m in _context.UlogaOsobe
                                             .Include(x => x.IdosobeNavigation)
                                             .Include(x => x.IdsadržajaNavigation)
                                             .Include(x => x.IdvrsteUlogeNavigation)
                                              orderby m.IdvrsteUlogeNavigation.Naziv
                                              select m.IdvrsteUlogeNavigation.Naziv;

            ViewBag.CurrentSort = sortOrder;

            ViewBag.osobaSortParam = String.IsNullOrEmpty(sortOrder) ? "osoba_desc" : "";
            ViewBag.sadrzajSortParam = sortOrder == "sadrzaj_desc" ? "sadrzaj_asc" : "sadrzaj_desc";
            ViewBag.vrstaUlogeSortParam = sortOrder == "vrstaUloge_desc" ? "vrstaUloge_asc" : "vrstaUloge_desc";


            if (osoba != null || sadrzaj != null || vrstaUloge != null)
            {
                page = 0;
            }

            if (osoba == null) osoba = currentOsoba;
            if (sadrzaj == null) sadrzaj = currentSadrzaj;
            if (vrstaUloge == null) vrstaUloge = currentVrstaUloge;

            ViewBag.CurrentOsoba = osoba;
            ViewBag.CurrentSadrzaj = sadrzaj;
            ViewBag.CurrentVrstaUloge = vrstaUloge;

            var uloge = from m in _context.UlogaOsobe
                .Include(u => u.IdosobeNavigation)
                .Include(u => u.IdsadržajaNavigation)
                .Include(u => u.IdvrsteUlogeNavigation)
                        select m;

            if (!String.IsNullOrEmpty(osoba))
            {
                uloge = uloge.Where(s => osoba.Equals(s.IdosobeNavigation.Ime +" "+ s.IdosobeNavigation.Prezime));
            }

            if (!String.IsNullOrEmpty(sadrzaj))
            {
                uloge = uloge.Where(x => x.IdsadržajaNavigation.Naziv == sadrzaj);
            }

            if (!String.IsNullOrEmpty(vrstaUloge))
            {
                uloge = uloge.Where(y => y.IdvrsteUlogeNavigation.Naziv == vrstaUloge);
            }

            switch (sortOrder)
            {

                case "osoba_desc":
                    uloge = uloge.OrderByDescending(s => s.IdosobeNavigation.Prezime);
                    break;
                case "sadrzaj_asc":
                    uloge = uloge.OrderBy(s => s.IdsadržajaNavigation.Naziv);
                    break;
                case "sadrzaj_desc":
                    uloge = uloge.OrderByDescending(s => s.IdsadržajaNavigation.Naziv);
                    break;
                case "vrstaUloge_asc":
                    uloge = uloge.OrderBy(s => s.IdvrsteUlogeNavigation.Naziv);
                    break;
                case "vrstaUloge_desc":
                    uloge = uloge.OrderByDescending(s => s.IdvrsteUlogeNavigation.Naziv);
                    break;
                default:
                    uloge = uloge.OrderBy(s => s.IdosobeNavigation.Prezime);
                    break;
            }

            const int pageSize = 3;
            var count = uloge.Count();
            if (count == 0) page = 0;
            else if ((page * pageSize) >= count) --page;

            uloge = uloge.Skip(page * pageSize).Take(pageSize);
            this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
            this.ViewBag.Page = page;

            var ulogaOsobeViewModel = new UlogaOsobeViewModel();

            ulogaOsobeViewModel.osobe = new SelectList(await osobaQuery.Distinct().ToListAsync());
            ulogaOsobeViewModel.sadrzaji = new SelectList(await sadrzajQuery.Distinct().ToListAsync());
            ulogaOsobeViewModel.vrsteUloga = new SelectList(await vrstaUlogeQuery.Distinct().ToListAsync());
            ulogaOsobeViewModel.ulogeOsoba = await uloge.ToListAsync();



            return View(ulogaOsobeViewModel);
        }

        // GET: UlogaOsobe/Details/5
        public async Task<IActionResult> Details(int? id, int? previousDetail, int? nextDetail, string currentVrstaUloge, string currentSadrzaj, string currentOsoba, string sortOrder, int page)
        {
            if (id == null && previousDetail == null && nextDetail == null)
            {
                return NotFound();
            }




            var ulog = from m in _context.UlogaOsobe
                .Include(u => u.IdosobeNavigation)
                .Include(u => u.IdsadržajaNavigation)
                .Include(u => u.IdvrsteUlogeNavigation)
                       select m;
            List<UlogaOsobe> uloge= await ulog.ToListAsync();


            if (previousDetail != null)
            {
                id = uloge.ElementAt((int)previousDetail).IdulogeOsobe;
            }

            if (nextDetail != null)
            {
                id = uloge.ElementAt((int)nextDetail).IdulogeOsobe;
            }


            var ulogaOsobe = await _context.UlogaOsobe
                .Include(u => u.IdosobeNavigation)
                .Include(u => u.IdsadržajaNavigation)
                .Include(u => u.IdvrsteUlogeNavigation)
                .AsNoTracking()
                .Where(m => m.IdulogeOsobe == id)
                .Select(s => new UlogaOsobeViewModel
                {
                    IdulogeOsobe=s.IdulogeOsobe,
                    IdvrsteUloge = s.IdvrsteUloge,
                    Idosobe = s.Idosobe,
                    Idsadržaja = s.Idsadržaja,
                    ImeIPrezimeOsobe = s.IdosobeNavigation.Ime + " " + s.IdosobeNavigation.Prezime,
                    NazivVrsteUloge=s.IdvrsteUlogeNavigation.Naziv,
                    NazivSadrzaja=s.IdsadržajaNavigation.Naziv
                }).FirstOrDefaultAsync();
                       


            if (ulogaOsobe == null)
            {
                return NotFound();
            }

            else
            {
                ulogaOsobe.ulogeOsoba = uloge;
                int indexAktualni = ulogaOsobe.ulogeOsoba.FindIndex(k => k.IdulogeOsobe== id);
                ViewBag.IndexAktualni = indexAktualni;
                ViewBag.NumberOfElements = ulogaOsobe.ulogeOsoba.Count();
            }

            ViewBag.Page = page;
            ViewBag.CurrentOsoba = currentOsoba;
            ViewBag.CurrentSadrzaj = currentSadrzaj;
            ViewBag.CurrentVrstaUloge = currentVrstaUloge;

            return View(ulogaOsobe);
        }

        // GET: UlogaOsobe/Create
        public IActionResult Create(string sortOrder, string currentFilter, string currentFormat, string currentVrsta, int page, int idSadrzaja = -1)
        {
            if (idSadrzaja == -1)
            {
                ViewBag.BackToSadrzaj = 0;
                ViewData["Idosobe"] = new SelectList(_context.Osoba, "Idosobe", "IP");
                ViewData["Idsadržaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv");
                ViewData["IdvrsteUloge"] = new SelectList(_context.VrstaUloge, "IdvrsteUloge", "Naziv");
                return View();
            }


            ViewData["Idosobe"] = new SelectList(_context.Osoba, "Idosobe", "IP");
            ViewData["Idsadržaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv",idSadrzaja);
            ViewData["IdvrsteUloge"] = new SelectList(_context.VrstaUloge, "IdvrsteUloge", "Naziv");

            ViewBag.BackToSadrzaj = 1;
            ViewBag.Page = page;
            ViewBag.CurrentFilter = currentFilter;
            ViewBag.CurrentFormat = currentFormat;
            ViewBag.CurrentVrsta = currentVrsta;
            ViewBag.CurrentSort = sortOrder;

            return View();

        }

        // POST: UlogaOsobe/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int backToSadrzaj,string sortOrder, string currentFilter, string currentFormat, string currentVrsta, int page,[Bind("IdulogeOsobe,IdvrsteUloge,Idosobe,Idsadržaja")] UlogaOsobe ulogaOsobe)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ulogaOsobe);
                await _context.SaveChangesAsync();
                if (backToSadrzaj==0) return RedirectToAction("Index");
                return RedirectToAction("Index","Sadrzaj", new { page = page, sortOrder = sortOrder, currentFilter = currentFilter, currentVrsta = currentVrsta });
            }
            ViewData["Idosobe"] = new SelectList(_context.Osoba, "Idosobe","IP", ulogaOsobe.Idosobe);
            ViewData["Idsadržaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", ulogaOsobe.Idsadržaja);
            ViewData["IdvrsteUloge"] = new SelectList(_context.VrstaUloge, "IdvrsteUloge", "Naziv", ulogaOsobe.IdvrsteUloge);
            return View(ulogaOsobe);
        }

        // GET: UlogaOsobe/Edit/5
        public async Task<IActionResult> Edit(int? id, string currentVrstaUloge, string currentSadrzaj, string currentOsoba, string sortOrder, int page)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ulogaOsobe = await _context.UlogaOsobe.SingleOrDefaultAsync(m => m.IdulogeOsobe == id);
            if (ulogaOsobe == null)
            {
                return NotFound();
            }
            ViewData["Idosobe"] = new SelectList(_context.Osoba, "Idosobe", "IP", ulogaOsobe.Idosobe);
            ViewData["Idsadržaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", ulogaOsobe.Idsadržaja);
            ViewData["IdvrsteUloge"] = new SelectList(_context.VrstaUloge, "IdvrsteUloge", "Naziv", ulogaOsobe.IdvrsteUloge);


            ViewBag.Page = page;
            ViewBag.CurrentOsoba= currentOsoba;
            ViewBag.CurrentSadrzaj = currentSadrzaj;
            ViewBag.CurrentVrstaUloge = currentVrstaUloge;
            ViewBag.CurrentSort = sortOrder;

            return View(ulogaOsobe);
        }

        // POST: UlogaOsobe/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, string currentVrstaUloge, string currentSadrzaj, string currentOsoba, string sortOrder, int page, [Bind("IdulogeOsobe,IdvrsteUloge,Idosobe,Idsadržaja")] UlogaOsobe ulogaOsobe)
        {
            if (id != ulogaOsobe.IdulogeOsobe)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ulogaOsobe);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UlogaOsobeExists(ulogaOsobe.IdulogeOsobe))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Edit", new { page = page, sortOrder = sortOrder, currentOsoba = currentOsoba, currentSadrzaj = currentSadrzaj, currentVrstaUloge = currentVrstaUloge});
            }
            ViewData["Idosobe"] = new SelectList(_context.Osoba, "Idosobe", "Ime", ulogaOsobe.Idosobe);
            ViewData["Idsadržaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", ulogaOsobe.Idsadržaja);
            ViewData["IdvrsteUloge"] = new SelectList(_context.VrstaUloge, "IdvrsteUloge", "IdvrsteUloge", ulogaOsobe.IdvrsteUloge);
            return View(ulogaOsobe);
        }

        // GET: UlogaOsobe/Delete/5
        public async Task<IActionResult> Delete(int? id, string sortOrder, string currentFilter, string currentFormat, string currentVrsta, int page, string currentVrstaUloge, string currentSadrzaj, string currentOsoba, int deleteFromSadrzaj = 0)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ulogaOsobe = await _context.UlogaOsobe
                .Include(u => u.IdosobeNavigation)
                .Include(u => u.IdsadržajaNavigation)
                .Include(u => u.IdvrsteUlogeNavigation)
                .SingleOrDefaultAsync(m => m.IdulogeOsobe == id);
            if (ulogaOsobe == null)
            {
                return NotFound();
            }

            //return View(ulogaOsobe);
            return await DeleteConfirmed((int)id, sortOrder, currentFilter, currentFormat, currentVrsta, page, deleteFromSadrzaj,currentVrstaUloge, currentSadrzaj, currentOsoba);
        }

        // POST: UlogaOsobe/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, string sortOrder, string currentFilter, string currentFormat, string currentVrsta, int page, int deleteFromSadrzaj, string currentVrstaUloge, string currentSadrzaj, string currentOsoba)
        {
            var ulogaOsobe = await _context.UlogaOsobe.SingleOrDefaultAsync(m => m.IdulogeOsobe == id);
            _context.UlogaOsobe.Remove(ulogaOsobe);
            await _context.SaveChangesAsync();

            if (deleteFromSadrzaj==0) return RedirectToAction("Index", new { page = page, sortOrder = sortOrder, currentOsoba = currentOsoba, currentVrstaUloge = currentVrstaUloge, currentSadrzaj = currentSadrzaj });
            return RedirectToAction("Index", "Sadrzaj", new { page = page, sortOrder = sortOrder, currentFilter = currentFilter, currentVrsta = currentVrsta });
        }

        private bool UlogaOsobeExists(int id)
        {
            return _context.UlogaOsobe.Any(e => e.IdulogeOsobe == id);
        }
    }
}
