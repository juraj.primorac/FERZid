using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class VrstaUredajaController : Controller
    {
        private readonly RPPP08Context _context;

        public VrstaUredajaController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: VrstaUredaja
        public async Task<IActionResult> Index(string sortOrder,string currentFilter,string searchString,int? page)
        {
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentFilter"] = searchString;
            var vrste = _context.VrstaUredaja
                .Include(v => v.Uredaj)
                .AsQueryable();
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                vrste = vrste.Where(u => u.Naziv.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    vrste = vrste.OrderByDescending(v => v.Naziv);
                    break;
                default:
                    vrste = vrste.OrderBy(v => v.Naziv);
                    break;
            }
            int pageSize = 20;
            return View(await PaginatedList<VrstaUredaja>.CreateAsync(vrste.AsNoTracking(), page ?? 1, pageSize));
        }

        // GET: VrstaUredaja/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vrstaUredaja = await _context.VrstaUredaja
                .SingleOrDefaultAsync(m => m.IdvrsteUredaja == id);
            if (vrstaUredaja == null)
            {
                return NotFound();
            }

            return View(vrstaUredaja);
        }

        // GET: VrstaUredaja/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: VrstaUredaja/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdvrsteUredaja,Naziv,Opis")] VrstaUredaja vrstaUredaja)
        {
            if (ModelState.IsValid)
            {
                _context.Add(vrstaUredaja);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(vrstaUredaja);
        }

        // GET: VrstaUredaja/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vrstaUredaja = await _context.VrstaUredaja.SingleOrDefaultAsync(m => m.IdvrsteUredaja == id);
            if (vrstaUredaja == null)
            {
                return NotFound();
            }
            return View(vrstaUredaja);
        }

        // POST: VrstaUredaja/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdvrsteUredaja,Naziv,Opis")] VrstaUredaja vrstaUredaja)
        {
            if (id != vrstaUredaja.IdvrsteUredaja)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vrstaUredaja);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VrstaUredajaExists(vrstaUredaja.IdvrsteUredaja))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(vrstaUredaja);
        }

        // GET: VrstaUredaja/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vrstaUredaja = await _context.VrstaUredaja
                .SingleOrDefaultAsync(m => m.IdvrsteUredaja == id);
            if (vrstaUredaja == null)
            {
                return NotFound();
            }

            return View(vrstaUredaja);
        }

        // POST: VrstaUredaja/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vrstaUredaja = await _context.VrstaUredaja.SingleOrDefaultAsync(m => m.IdvrsteUredaja == id);
            _context.VrstaUredaja.Remove(vrstaUredaja);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool VrstaUredajaExists(int id)
        {
            return _context.VrstaUredaja.Any(e => e.IdvrsteUredaja == id);
        }
    }
}
