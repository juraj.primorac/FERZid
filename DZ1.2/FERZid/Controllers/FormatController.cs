using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;


namespace FERZid.Controllers
{
    public class FormatController : Controller
    {
        private readonly RPPP08Context _context;

        public FormatController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: Format
        public async Task<IActionResult> Index(string searchString, string currentFilter, string sortOrder,string errorMessage,string successMessage, int page=0)
        {

            ViewBag.CurrentSort = sortOrder;
            ViewBag.nazivSortParam = String.IsNullOrEmpty(sortOrder) ? "naziv_desc" : "";

            if (searchString != null)
            {
                page = 0;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var form = from m in _context.Format
                       select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                form = form.Where(y => y.Naziv.Contains(searchString));
            }


            switch (sortOrder)
            {

                case "naziv_desc":
                    form = form.OrderByDescending(s => s.Naziv);
                    break;
              
                default:
                    form = form.OrderBy(s => s.Naziv);
                    break;
            }

            const int pageSize = 3;
            var count = form.Count();
            if (count == 0) page = 0;
            else if ((page * pageSize) >= count) --page;


            form = form.Skip(page * pageSize).Take(pageSize);
            this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
            this.ViewBag.Page = page;


            var formatViewModel = new FormatViewModel();

            
            formatViewModel.formati = await form.ToListAsync();

            ViewData["Error"] = errorMessage;
            TempData["Success"] = successMessage;

            return View(formatViewModel);
        }

        // GET: Format/Details/5
        public async Task<IActionResult> Details(int? id, int? previousDetail, int? nextDetail, string sortOrder, string currentFilter, int page)
        {
            if (id == null)
            {
                return NotFound();
            }

            var form = from m in _context.Format
                       select m;
            List<Format> formati = await form.ToListAsync();

            if (previousDetail != null)
            {
                id = formati.ElementAt((int)previousDetail).Idformata;
            }

            if (nextDetail != null)
            {
                id = formati.ElementAt((int)nextDetail).Idformata;
            }








            var format = await _context.Format
                .AsNoTracking()
                .Where(m => m.Idformata == id)
                .Select(f => new FormatViewModel
                {

                    Idformata = f.Idformata,
                    Naziv = f.Naziv
                })
                .FirstOrDefaultAsync();


            if (format == null)
            {
                return NotFound();
            }

            else
            {
                format.formati = formati;
                int indexAktualni = format.formati.FindIndex(f => f.Idformata == id);
                ViewBag.IndexAktualni = indexAktualni;
                ViewBag.NumberOfElements = format.formati.Count();
            }

            ViewBag.Page = page;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentFilter = currentFilter;

            

            return View(format);
        }

        // GET: Format/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Format/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idformata,Naziv")] Format format)
        {
            string successMessage=null;

            if (ModelState.IsValid)
            {
                _context.Add(format);
                await _context.SaveChangesAsync();

                successMessage = "Format created: " + format.Naziv;
                return RedirectToAction("Index",new { successMessage = successMessage });
            }
            return View(format);
        }

        // GET: Format/Edit/5
        public async Task<IActionResult> Edit(int? id, string sortOrder, string currentFilter, int page, string successMessage)
        {
            if (id == null)
            {
                return NotFound();
            }

            var format = await _context.Format.SingleOrDefaultAsync(m => m.Idformata == id);
            if (format == null)
            {
                return NotFound();
            }

            ViewBag.Page = page;
            ViewBag.CurrentFilter = currentFilter;
            ViewBag.CurrentSort = sortOrder;

            if (successMessage != null) TempData["Success"] = successMessage;
            return View(format);
        }

        // POST: Format/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, string sortOrder, string currentFilter, int page, [Bind("Idformata,Naziv")] Format format)
        {
            string successMessage = null;

            if (id != format.Idformata)
            {
                return NotFound();
            }



            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(format);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FormatExists(format.Idformata))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }

                }
                successMessage= "Format edited: " + format.Naziv;
                return RedirectToAction("Edit", new { page = page, sortOrder = sortOrder, currentFilter = currentFilter,successMessage=successMessage });
            }
            return View(format);
        }

        // GET: Format/Delete/5
        public async Task<IActionResult> Delete(int? id, string sortOrder, string currentFilter, int page)
        {
            if (id == null)
            {
                return NotFound();
            }

            var format = await _context.Format
                .SingleOrDefaultAsync(m => m.Idformata == id);
            if (format == null)
            {
                return NotFound();
            }

            //return View(format);
            return await DeleteConfirmed((int)id, sortOrder, currentFilter, page);
        }

        // POST: Format/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, string sortOrder, string currentFilter, int page)
        {
            string errorMessage=null;
            string successMessage = null;

            var format = await _context.Format.SingleOrDefaultAsync(m => m.Idformata == id);
            _context.Format.Remove(format);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e) {
                errorMessage = "Format delete failed:  "+format.Naziv;
            }

            if (errorMessage == null) successMessage = "Format deleted: " + format.Naziv;
            return RedirectToAction("Index", new { page = page, sortOrder = sortOrder, currentFilter = currentFilter,errorMessage= errorMessage,successMessage=successMessage });
        }

        private bool FormatExists(int id)
        {
            return _context.Format.Any(e => e.Idformata == id);
        }
    }
}
