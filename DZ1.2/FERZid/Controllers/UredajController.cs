using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class UredajController : Controller
    {
        private readonly RPPP08Context _context;

        public UredajController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: Uredaj
        public async Task<IActionResult> Index(string sortOrder,string searchString, string currentFilter,int? page)
        {
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentFilter"] = searchString;
            var uredaji = _context.Uredaj
                .Include(u => u.IdvrsteUredajaNavigation)
                .Include(u => u.IdzamjenskiUredajNavigation)
                .Include(u => u.Komponenta).ThenInclude((Komponenta k)=>k.IdtipKomponenteNavigation)
                .Include(u => u.Servis).ThenInclude((Servis s)=>s.IdvrsteNavigation)
                .AsQueryable();
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                uredaji = uredaji.Where(u => u.ImeUredaja.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    uredaji = uredaji.OrderByDescending(u => u.ImeUredaja);
                    break;
                default:
                    uredaji = uredaji.OrderBy(u => u.ImeUredaja);
                    break;

            }
            int pageSize = 20;
            return View(await PaginatedList<Uredaj>.CreateAsync(uredaji.AsNoTracking(), page ?? 1, pageSize));

        }

        // GET: Uredaj/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uredaj = await _context.Uredaj
                .Include(u => u.IdvrsteUredajaNavigation)
                .Include(u => u.IdzamjenskiUredajNavigation)
                .Include(u => u.Komponenta).ThenInclude(k => k.IdtipKomponenteNavigation)
                .SingleOrDefaultAsync(m => m.Iduredaja == id);
            if (uredaj == null)
            {
                return NotFound();
            }

            return View(uredaj);
        }

        // GET: Uredaj/Create
        public IActionResult Create()
        {
            ViewData["IdvrsteUredaja"] = new SelectList(_context.VrstaUredaja, "IdvrsteUredaja", "Naziv");
            ViewData["IdzamjenskiUredaj"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja");
            return View();
        }

        // POST: Uredaj/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Iduredaja,ImeUredaja,IdvrsteUredaja,IdzamjenskiUredaj")] Uredaj uredaj)
        {
            if (ModelState.IsValid)
            {
                _context.Add(uredaj);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdvrsteUredaja"] = new SelectList(_context.VrstaUredaja, "IdvrsteUredaja", "Naziv", uredaj.IdvrsteUredaja);
            ViewData["IdzamjenskiUredaj"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja", uredaj.IdzamjenskiUredaj);
            return View(uredaj);
        }

        // GET: Uredaj/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uredaj = await _context.Uredaj.SingleOrDefaultAsync(m => m.Iduredaja == id);
            if (uredaj == null)
            {
                return NotFound();
            }
            ViewData["IdvrsteUredaja"] = new SelectList(_context.VrstaUredaja, "IdvrsteUredaja", "Naziv", uredaj.IdvrsteUredaja);
            ViewData["IdzamjenskiUredaj"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja", uredaj.IdzamjenskiUredaj);
            return View(uredaj);
        }

        // POST: Uredaj/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Iduredaja,ImeUredaja,IdvrsteUredaja,IdzamjenskiUredaj")] Uredaj uredaj)
        {
            if (id != uredaj.Iduredaja)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(uredaj);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UredajExists(uredaj.Iduredaja))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdvrsteUredaja"] = new SelectList(_context.VrstaUredaja, "IdvrsteUredaja", "Naziv", uredaj.IdvrsteUredaja);
            ViewData["IdzamjenskiUredaj"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja", uredaj.IdzamjenskiUredaj);
            return View(uredaj);
        }

        // GET: Uredaj/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uredaj = await _context.Uredaj
                .Include(u => u.IdvrsteUredajaNavigation)
                .Include(u => u.IdzamjenskiUredajNavigation)
                .SingleOrDefaultAsync(m => m.Iduredaja == id);
            if (uredaj == null)
            {
                return NotFound();
            }

            return View(uredaj);
        }

        // POST: Uredaj/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var uredaj = await _context.Uredaj.SingleOrDefaultAsync(m => m.Iduredaja == id);
            _context.Uredaj.Remove(uredaj);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool UredajExists(int id)
        {
            return _context.Uredaj.Any(e => e.Iduredaja == id);
        }
    }
}
