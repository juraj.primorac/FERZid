using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class PovijestInstanciPrikazivanjasController : Controller
    {
        private readonly RPPP08Context _context;

        public PovijestInstanciPrikazivanjasController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: PovijestInstanciPrikazivanjas
        public async Task<IActionResult> Index()
        {
            var rPPP08Context = _context.PovijestInstanciPrikazivanja.Include(p => p.IdsadržajaNavigation);
            return View(await rPPP08Context.ToListAsync());
        }

        // GET: PovijestInstanciPrikazivanjas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var povijestInstanciPrikazivanja = await _context.PovijestInstanciPrikazivanja
                .Include(p => p.IdsadržajaNavigation)
                .SingleOrDefaultAsync(m => m.IdInstancePrikazivanja == id);
            if (povijestInstanciPrikazivanja == null)
            {
                return NotFound();
            }

            return View(povijestInstanciPrikazivanja);
        }

        // GET: PovijestInstanciPrikazivanjas/Create
        public IActionResult Create()
        {
            ViewData["Idsadržaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv");
            return View();
        }

        // POST: PovijestInstanciPrikazivanjas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("VrijemePrikazivanja,Idsadržaja,Idzida,IdInstancePrikazivanja")] PovijestInstanciPrikazivanja povijestInstanciPrikazivanja)
        {
            if (ModelState.IsValid)
            {
                _context.Add(povijestInstanciPrikazivanja);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["Idsadržaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", povijestInstanciPrikazivanja.Idsadržaja);
            return View(povijestInstanciPrikazivanja);
        }

        // GET: PovijestInstanciPrikazivanjas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var povijestInstanciPrikazivanja = await _context.PovijestInstanciPrikazivanja.SingleOrDefaultAsync(m => m.IdInstancePrikazivanja == id);
            if (povijestInstanciPrikazivanja == null)
            {
                return NotFound();
            }
            ViewData["Idsadržaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", povijestInstanciPrikazivanja.Idsadržaja);
            return View(povijestInstanciPrikazivanja);
        }

        // POST: PovijestInstanciPrikazivanjas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("VrijemePrikazivanja,Idsadržaja,Idzida,IdInstancePrikazivanja")] PovijestInstanciPrikazivanja povijestInstanciPrikazivanja)
        {
            if (id != povijestInstanciPrikazivanja.IdInstancePrikazivanja)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(povijestInstanciPrikazivanja);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PovijestInstanciPrikazivanjaExists(povijestInstanciPrikazivanja.IdInstancePrikazivanja))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["Idsadržaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", povijestInstanciPrikazivanja.Idsadržaja);
            return View(povijestInstanciPrikazivanja);
        }

        // GET: PovijestInstanciPrikazivanjas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var povijestInstanciPrikazivanja = await _context.PovijestInstanciPrikazivanja
                .Include(p => p.IdsadržajaNavigation)
                .SingleOrDefaultAsync(m => m.IdInstancePrikazivanja == id);
            if (povijestInstanciPrikazivanja == null)
            {
                return NotFound();
            }

            return View(povijestInstanciPrikazivanja);
        }

        // POST: PovijestInstanciPrikazivanjas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var povijestInstanciPrikazivanja = await _context.PovijestInstanciPrikazivanja.SingleOrDefaultAsync(m => m.IdInstancePrikazivanja == id);
            _context.PovijestInstanciPrikazivanja.Remove(povijestInstanciPrikazivanja);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PovijestInstanciPrikazivanjaExists(int id)
        {
            return _context.PovijestInstanciPrikazivanja.Any(e => e.IdInstancePrikazivanja == id);
        }
    }
}
