using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class TipKomponenteController : Controller
    {
        private readonly RPPP08Context _context;

        public TipKomponenteController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: TipKomponente
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString,int? page)
        {
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentFilter"] = searchString;
            var tipovi = _context.TipKomponente
                .Include(t =>t.Komponenta)
                .AsQueryable();
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                tipovi = tipovi.Where(u => u.Naziv.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    tipovi = tipovi.OrderByDescending(t => t.Naziv);
                    break;
                default:
                    tipovi = tipovi.OrderBy(t => t.Naziv);
                    break;
            }
            int pageSize = 5;
            return View(await PaginatedList<TipKomponente>.CreateAsync(tipovi.AsNoTracking(), page ?? 1, pageSize));
        }

        // GET: TipKomponente/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipKomponente = await _context.TipKomponente
                .SingleOrDefaultAsync(m => m.IdtipKomponente == id);
            if (tipKomponente == null)
            {
                return NotFound();
            }

            return View(tipKomponente);
        }

        // GET: TipKomponente/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipKomponente/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdtipKomponente,Naziv,Opis")] TipKomponente tipKomponente)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipKomponente);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tipKomponente);
        }

        // GET: TipKomponente/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipKomponente = await _context.TipKomponente.SingleOrDefaultAsync(m => m.IdtipKomponente == id);
            if (tipKomponente == null)
            {
                return NotFound();
            }
            return View(tipKomponente);
        }

        // POST: TipKomponente/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdtipKomponente,Naziv,Opis")] TipKomponente tipKomponente)
        {
            if (id != tipKomponente.IdtipKomponente)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipKomponente);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipKomponenteExists(tipKomponente.IdtipKomponente))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(tipKomponente);
        }

        // GET: TipKomponente/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipKomponente = await _context.TipKomponente
                .SingleOrDefaultAsync(m => m.IdtipKomponente == id);
            if (tipKomponente == null)
            {
                return NotFound();
            }

            return View(tipKomponente);
        }

        // POST: TipKomponente/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipKomponente = await _context.TipKomponente.SingleOrDefaultAsync(m => m.IdtipKomponente == id);
            _context.TipKomponente.Remove(tipKomponente);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool TipKomponenteExists(int id)
        {
            return _context.TipKomponente.Any(e => e.IdtipKomponente == id);
        }
    }
}
