using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class SadrzajNaSegmentuController : Controller
    {
        private readonly RPPP08Context _context;

        public SadrzajNaSegmentuController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: SadrzajNaSegmentu
        public async Task<IActionResult> Index()
        {
            var rPPP08Context = _context.SadrzajNaSegmentu.Include(s => s.IdsadrzajaNavigation).Include(s => s.IdsegmentaNavigation).
                Include(s => s.IdsegmentaNavigation.IdzidaNavigation);
            return View(await rPPP08Context.ToListAsync());
        }

        // GET: SadrzajNaSegmentu/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sadrzajNaSegmentu = await _context.SadrzajNaSegmentu
                .Include(s => s.IdsadrzajaNavigation)
                .Include(s => s.IdsegmentaNavigation)
                .Include(s => s.IdsegmentaNavigation.IdzidaNavigation)
                .SingleOrDefaultAsync(m => m.IdsnaS == id);
            if (sadrzajNaSegmentu == null)
            {
                return NotFound();
            }

            return View(sadrzajNaSegmentu);
        }

        // GET: SadrzajNaSegmentu/Create
        public IActionResult Create()
        {
            ViewData["Idsadrzaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv");
            ViewData["Idsegmenta"] = new SelectList(_context.Segment, "Idsegmenta", "Idsegmenta");
            return View();
        }

        // POST: SadrzajNaSegmentu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdsnaS,Idsegmenta,Idsadrzaja")] SadrzajNaSegmentu sadrzajNaSegmentu)
        {
            if (ModelState.IsValid)
            {
                _context.Add(sadrzajNaSegmentu);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["Idsadrzaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", sadrzajNaSegmentu.Idsadrzaja);
            ViewData["Idsegmenta"] = new SelectList(_context.Segment, "Idsegmenta", "Idsegmenta", sadrzajNaSegmentu.Idsegmenta);
            return View(sadrzajNaSegmentu);
        }

        // GET: SadrzajNaSegmentu/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sadrzajNaSegmentu = await _context.SadrzajNaSegmentu.SingleOrDefaultAsync(m => m.IdsnaS == id);
            if (sadrzajNaSegmentu == null)
            {
                return NotFound();
            }
            ViewData["Idsadrzaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", sadrzajNaSegmentu.Idsadrzaja);
            ViewData["Idsegmenta"] = new SelectList(_context.Segment, "Idsegmenta", "Idsegmenta", sadrzajNaSegmentu.Idsegmenta);
            return View(sadrzajNaSegmentu);
        }

        // POST: SadrzajNaSegmentu/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdsnaS,Idsegmenta,Idsadrzaja")] SadrzajNaSegmentu sadrzajNaSegmentu)
        {
            if (id != sadrzajNaSegmentu.IdsnaS)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(sadrzajNaSegmentu);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SadrzajNaSegmentuExists(sadrzajNaSegmentu.IdsnaS))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["Idsadrzaja"] = new SelectList(_context.Sadržaj, "Idsadržaja", "Naziv", sadrzajNaSegmentu.Idsadrzaja);
            ViewData["Idsegmenta"] = new SelectList(_context.Segment, "Idsegmenta", "Idsegmenta", sadrzajNaSegmentu.Idsegmenta);
            return View(sadrzajNaSegmentu);
        }

        // GET: SadrzajNaSegmentu/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sadrzajNaSegmentu = await _context.SadrzajNaSegmentu
                .Include(s => s.IdsadrzajaNavigation)
                .Include(s => s.IdsegmentaNavigation)
                .Include(s => s.IdsegmentaNavigation.IdzidaNavigation)
                .SingleOrDefaultAsync(m => m.IdsnaS == id);
            if (sadrzajNaSegmentu == null)
            {
                return NotFound();
            }

            return View(sadrzajNaSegmentu);
        }

        // POST: SadrzajNaSegmentu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sadrzajNaSegmentu = await _context.SadrzajNaSegmentu.SingleOrDefaultAsync(m => m.IdsnaS == id);
            _context.SadrzajNaSegmentu.Remove(sadrzajNaSegmentu);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool SadrzajNaSegmentuExists(int id)
        {
            return _context.SadrzajNaSegmentu.Any(e => e.IdsnaS == id);
        }
    }
}
