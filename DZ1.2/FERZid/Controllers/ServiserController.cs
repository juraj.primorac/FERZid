using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;
using FERZid.ViewModels;

namespace FERZid.Controllers
{
    public class ServiserController : Controller
    {
        private readonly RPPP08Context _context;

        public ServiserController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: Serviser
        public async Task<IActionResult> Index()
        {
            var rPPP08Context = _context.Serviser
                .Include(s => s.IdbankeNavigation)
                .AsNoTracking()
                .OrderBy(s => s.Ime);
            return View(await rPPP08Context.ToListAsync());
        }

        // GET: Serviser/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var serviser = await _context.Serviser
                .AsNoTracking()
                .Where(s => s.Idservisera == id)
                .Select(s => new ServiserViewModel
                {
                    Idservisera = s.Idservisera,
                    Ime = s.Ime,
                    Prezime = s.Prezime,
                    Oib = s.Oib,
                    Email = s.Email,
                    Sjediste = s.Sjediste,
                    Ziroracun = s.Ziroracun,
                    Idbanke = s.IdbankeNavigation.Idbanke,
                    NazivBanke = s.IdbankeNavigation.Naziv,
                    IbanBanke = s.IdbankeNavigation.Iban,
                    SwiftBanke = s.IdbankeNavigation.Swift,
                    SjedisteBanke = s.IdbankeNavigation.Sjediste
                })
                .FirstOrDefaultAsync();
            if (serviser == null)
            {
                return NotFound();
            }
            else
            {
                var servisi = await _context.UlogaServisera
                    .Where(s => s.Idservisera == serviser.Idservisera)
                    .Select(s => new UlogaServiseraViewModel
                    {
                        Idservisa = s.Idservisa,
                        ImeUredaja = s.IdservisaNavigation.IduredajaNavigation.ImeUredaja,
                        Idservisera = s.Idservisera,
                        Uloga = s.Uloga,
                        Zapoceo = s.Zapoceo,
                        Zavrsio = s.Zavrsio
                    })
                    .OrderBy(s => s.Idservisa)
                    .ToListAsync();

                serviser.Servisi = servisi;
            }

            return View(serviser);
        }

        // GET: Serviser/Create
        public IActionResult Create()
        {
            ViewData["Idbanke"] = new SelectList(_context.Banka, "Idbanke", "Naziv");
            return View();
        }

        // POST: Serviser/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idservisera,Ime,Prezime,Oib,Email,Sjediste,Ziroracun,Idbanke")] Serviser serviser)
        {
            if (ModelState.IsValid)
            {
                _context.Add(serviser);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["Idbanke"] = new SelectList(_context.Banka, "Idbanke", "Iban", serviser.Idbanke);
            return View(serviser);
        }

        // GET: Serviser/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var serviser = await _context.Serviser.SingleOrDefaultAsync(m => m.Idservisera == id);
            if (serviser == null)
            {
                return NotFound();
            }
            ViewData["Idbanke"] = new SelectList(_context.Banka, "Idbanke", "Iban", serviser.Idbanke);
            return View(serviser);
        }

        // POST: Serviser/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idservisera,Ime,Prezime,Oib,Email,Sjediste,Ziroracun,Idbanke")] Serviser serviser)
        {
            if (id != serviser.Idservisera)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(serviser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ServiserExists(serviser.Idservisera))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["Idbanke"] = new SelectList(_context.Banka, "Idbanke", "Iban", serviser.Idbanke);
            return View(serviser);
        }

        // GET: Serviser/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var serviser = await _context.Serviser
                .Include(s => s.IdbankeNavigation)
                .SingleOrDefaultAsync(m => m.Idservisera == id);
            if (serviser == null)
            {
                return NotFound();
            }

            return View(serviser);
        }

        // POST: Serviser/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var serviser = await _context.Serviser.SingleOrDefaultAsync(m => m.Idservisera == id);
            _context.Serviser.Remove(serviser);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool ServiserExists(int id)
        {
            return _context.Serviser.Any(e => e.Idservisera == id);
        }
    }
}
