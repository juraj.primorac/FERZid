using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class VrstaSadrzajaController : Controller
    {
        private readonly RPPP08Context _context;

        public VrstaSadrzajaController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: VrstaSadrzaja
        public async Task<IActionResult> Index(string searchString, string sortOrder,string currentFilter, string errorMessage, string successMessage, int page=0)
        {


            ViewBag.CurrentSort = sortOrder;

            ViewBag.nazivSortParam = String.IsNullOrEmpty(sortOrder) ? "naziv_desc" : "";

            if (searchString != null)
            {
                page = 0;
            }

            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var vrst = from m in _context.VrstaSadržaja
                       select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                vrst = vrst.Where(s => s.Naziv.Contains(searchString));
            }

            switch (sortOrder)
            {

                case "naziv_desc":
                   vrst = vrst.OrderByDescending(s => s.Naziv);
                    break;

                default:
                    vrst= vrst.OrderBy(s => s.Naziv);
                    break;
            }

            const int pageSize = 3;
            var count = vrst.Count();
            if (count == 0) page = 0;
            else if ((page * pageSize) >= count) --page;

            vrst = vrst.Skip(page * pageSize).Take(pageSize);
            this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
            this.ViewBag.Page = page;

            var vrstaSadrzajaViewModel = new VrstaSadrzajaViewModel();


            vrstaSadrzajaViewModel.vrsteSadrzaja= await vrst.ToListAsync();

            ViewData["Error"] = errorMessage;
            TempData["Success"] = successMessage;

            return View(vrstaSadrzajaViewModel);
        }

        // GET: VrstaSadrzaja/Details/5
        public async Task<IActionResult> Details(int? id, int? previousDetail, int? nextDetail, string sortOrder, string currentFilter, int page)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vrst = from m in _context.VrstaSadržaja
                       select m;
            List<VrstaSadržaja> vrste = await vrst.ToListAsync();

            if (previousDetail != null)
            {
                id = vrste.ElementAt((int)previousDetail).IdvrsteSadržaja;
            }

            if (nextDetail != null)
            {
                id = vrste.ElementAt((int)nextDetail).IdvrsteSadržaja;
            }


            var vrstaSadrzaja = await _context.VrstaSadržaja
               .AsNoTracking()
               .Where(m => m.IdvrsteSadržaja == id)
               .Select(s => new VrstaSadrzajaViewModel
               {
                   IdvrsteSadržaja = s.IdvrsteSadržaja,
                   Naziv = s.Naziv,
                   Opis= s.Opis
               })
               .FirstOrDefaultAsync();




            if (vrstaSadrzaja == null)
            {
                return NotFound();
            }
            else
            {
                vrstaSadrzaja.vrsteSadrzaja = vrste;
                int indexAktualni = vrstaSadrzaja.vrsteSadrzaja.FindIndex(v => v.IdvrsteSadržaja == id);
                ViewBag.IndexAktualni = indexAktualni;
                ViewBag.NumberOfElements = vrstaSadrzaja.vrsteSadrzaja.Count();
            }

            ViewBag.Page = page;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentFilter = currentFilter;

            return View(vrstaSadrzaja);
        }

        // GET: VrstaSadrzaja/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: VrstaSadrzaja/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdvrsteSadržaja,Naziv,Opis")] VrstaSadržaja vrstaSadržaja)
        {
            string successMessage = null;

            if (ModelState.IsValid)
            {
                _context.Add(vrstaSadržaja);
                await _context.SaveChangesAsync();
                successMessage = "Vrsta sadržaja created: " + vrstaSadržaja.Naziv;
                return RedirectToAction("Index", new { successMessage = successMessage });
            }
            return View(vrstaSadržaja);
        }

        // GET: VrstaSadrzaja/Edit/5
        public async Task<IActionResult> Edit(int? id, string sortOrder, string currentFilter, int page, string successMessage)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vrstaSadržaja = await _context.VrstaSadržaja.SingleOrDefaultAsync(m => m.IdvrsteSadržaja == id);
            if (vrstaSadržaja == null)
            {
                return NotFound();
            }

            ViewBag.Page = page;
            ViewBag.CurrentFilter = currentFilter;
            ViewBag.CurrentSort = sortOrder;

            if (successMessage != null) TempData["Success"] = successMessage;
            return View(vrstaSadržaja);
        }

        // POST: VrstaSadrzaja/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, string sortOrder, string currentFilter, int page, [Bind("IdvrsteSadržaja,Naziv,Opis")] VrstaSadržaja vrstaSadržaja)
        {

            string successMessage = null;

            if (id != vrstaSadržaja.IdvrsteSadržaja)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vrstaSadržaja);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VrstaSadržajaExists(vrstaSadržaja.IdvrsteSadržaja))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                successMessage = "Vrsta sadržaja edited: " + vrstaSadržaja.Naziv;
                return RedirectToAction("Edit", new { page = page, sortOrder = sortOrder, currentFilter = currentFilter, successMessage = successMessage });
            }
            return View(vrstaSadržaja);
        }

        // GET: VrstaSadrzaja/Delete/5
        public async Task<IActionResult> Delete(int? id, string sortOrder, string currentFilter, int page)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vrstaSadržaja = await _context.VrstaSadržaja
                .SingleOrDefaultAsync(m => m.IdvrsteSadržaja == id);
            if (vrstaSadržaja == null)
            {
                return NotFound();
            }

            //return View(vrstaSadržaja);
            return await DeleteConfirmed((int)id, sortOrder, currentFilter, page);
        }

        // POST: VrstaSadrzaja/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, string sortOrder, string currentFilter, int page)
        {
            string errorMessage = null;
            string successMessage = null;

            var vrstaSadržaja = await _context.VrstaSadržaja.SingleOrDefaultAsync(m => m.IdvrsteSadržaja == id);
            _context.VrstaSadržaja.Remove(vrstaSadržaja);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                errorMessage = "Vrsta sadržaja delete failed:  " + vrstaSadržaja.Naziv;
            }

            if (errorMessage == null) successMessage = "Format deleted: " + vrstaSadržaja.Naziv;
            return RedirectToAction("Index", new { page = page, sortOrder = sortOrder, currentFilter = currentFilter, errorMessage = errorMessage, successMessage = successMessage });
        }

        private bool VrstaSadržajaExists(int id)
        {
            return _context.VrstaSadržaja.Any(e => e.IdvrsteSadržaja == id);
        }
    }
}
