using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class TipProgramasController : Controller
    {
        private readonly RPPP08Context _context;

        public TipProgramasController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: TipProgramas
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipPrograma.ToListAsync());
        }

        // GET: TipProgramas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipPrograma = await _context.TipPrograma
                .SingleOrDefaultAsync(m => m.IdTipaPrograma == id);
            if (tipPrograma == null)
            {
                return NotFound();
            }

            return View(tipPrograma);
        }

        // GET: TipProgramas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipProgramas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdTipaPrograma,NazivTipaPrograma,Kontekst")] TipPrograma tipPrograma)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipPrograma);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tipPrograma);
        }

        // GET: TipProgramas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipPrograma = await _context.TipPrograma.SingleOrDefaultAsync(m => m.IdTipaPrograma == id);
            if (tipPrograma == null)
            {
                return NotFound();
            }
            return View(tipPrograma);
        }

        // POST: TipProgramas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTipaPrograma,NazivTipaPrograma,Kontekst")] TipPrograma tipPrograma)
        {
            if (id != tipPrograma.IdTipaPrograma)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipPrograma);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipProgramaExists(tipPrograma.IdTipaPrograma))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(tipPrograma);
        }

        // GET: TipProgramas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipPrograma = await _context.TipPrograma
                .SingleOrDefaultAsync(m => m.IdTipaPrograma == id);
            if (tipPrograma == null)
            {
                return NotFound();
            }

            return View(tipPrograma);
        }

        // POST: TipProgramas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipPrograma = await _context.TipPrograma.SingleOrDefaultAsync(m => m.IdTipaPrograma == id);
            _context.TipPrograma.Remove(tipPrograma);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool TipProgramaExists(int id)
        {
            return _context.TipPrograma.Any(e => e.IdTipaPrograma == id);
        }
    }
}
