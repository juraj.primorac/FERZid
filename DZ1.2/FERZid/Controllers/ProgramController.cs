using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class ProgramController : Controller
    {
        private readonly RPPP08Context _context;

        public ProgramController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: Program
        public async Task<IActionResult> Index()
        {
            var rPPP08Context = _context.Program.Include(p => p.IdTipaProgramaNavigation);
            return View(await rPPP08Context.ToListAsync());
        }

        // GET: Program/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var program = await _context.Program
                .Include(p => p.IdTipaProgramaNavigation)
                .SingleOrDefaultAsync(m => m.IdPrograma == id);
            if (program == null)
            {
                return NotFound();
            }

            return View(program);
        }

        // GET: Program/Create
        public IActionResult Create()
        {
            ViewData["IdTipaPrograma"] = new SelectList(_context.TipPrograma, "IdTipaPrograma", "NazivTipaPrograma");
            return View();
        }

        // POST: Program/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdPrograma,ImePrograma,IdTipaPrograma,Trajanje")] Program program)
        {
            if (ModelState.IsValid)
            {
                _context.Add(program);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdTipaPrograma"] = new SelectList(_context.TipPrograma, "IdTipaPrograma", "NazivTipaPrograma", program.IdTipaPrograma);
            return View(program);
        }

        // GET: Program/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var program = await _context.Program.SingleOrDefaultAsync(m => m.IdPrograma == id);
            if (program == null)
            {
                return NotFound();
            }
            ViewData["IdTipaPrograma"] = new SelectList(_context.TipPrograma, "IdTipaPrograma", "NazivTipaPrograma", program.IdTipaPrograma);
            return View(program);
        }

        // POST: Program/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdPrograma,ImePrograma,IdTipaPrograma,Trajanje")] Program program)
        {
            if (id != program.IdPrograma)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(program);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProgramExists(program.IdPrograma))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdTipaPrograma"] = new SelectList(_context.TipPrograma, "IdTipaPrograma", "NazivTipaPrograma", program.IdTipaPrograma);
            return View(program);
        }

        // GET: Program/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var program = await _context.Program
                .Include(p => p.IdTipaProgramaNavigation)
                .SingleOrDefaultAsync(m => m.IdPrograma == id);
            if (program == null)
            {
                return NotFound();
            }

            return View(program);
        }

        // POST: Program/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var program = await _context.Program.SingleOrDefaultAsync(m => m.IdPrograma == id);
            _context.Program.Remove(program);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool ProgramExists(int id)
        {
            return _context.Program.Any(e => e.IdPrograma == id);
        }
    }
}
