using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class ProgramAktivanController : Controller
    {
        private readonly RPPP08Context _context;

        public ProgramAktivanController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: ProgramAktivan
        public async Task<IActionResult> Index()
        {
            var rPPP08Context = _context.ProgramAktivan.Include(p => p.IdzidaNavigation).Include(p => p.IdProgramaNavigation);
            return View(await rPPP08Context.ToListAsync());
        }

        // GET: ProgramAktivan/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var programAktivan = await _context.ProgramAktivan
                .Include(p => p.IdzidaNavigation)
                .Include(p => p.IdProgramaNavigation)
                .SingleOrDefaultAsync(m => m.IdprogramAktivan == id);
            if (programAktivan == null)
            {
                return NotFound();
            }

            return View(programAktivan);
        }

        // GET: ProgramAktivan/Create
        public IActionResult Create()
        {
            ViewData["Idzida"] = new SelectList(_context.Zid, "Idzida", "Lokacija");
            //Janko nadodao 26.04.2017.
            ViewData["IdPrograma"] = new SelectList(_context.Program, "IdPrograma", "ImePrograma");
            return View();
        }

        // POST: ProgramAktivan/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdprogramAktivan,Idzida,Idprograma,Pocetak,Kraj")] ProgramAktivan programAktivan)
        {
            if (ModelState.IsValid)
            {
                _context.Add(programAktivan);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["Idzida"] = new SelectList(_context.Zid, "Idzida", "Lokacija", programAktivan.Idzida);
            return View(programAktivan);
        }

        // GET: ProgramAktivan/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var programAktivan = await _context.ProgramAktivan.SingleOrDefaultAsync(m => m.IdprogramAktivan == id);
            if (programAktivan == null)
            {
                return NotFound();
            }
            ViewData["Idzida"] = new SelectList(_context.Zid, "Idzida", "Lokacija", programAktivan.Idzida);
            //Janko nadodao 27.04.2017.
            ViewData["IdPrograma"] = new SelectList(_context.Program, "IdPrograma", "ImePrograma");
            return View(programAktivan);
        }

        // POST: ProgramAktivan/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdprogramAktivan,Idzida,Idprograma,Pocetak,Kraj")] ProgramAktivan programAktivan)
        {
            if (id != programAktivan.IdprogramAktivan)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(programAktivan);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProgramAktivanExists(programAktivan.IdprogramAktivan))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["Idzida"] = new SelectList(_context.Zid, "Idzida", "Lokacija", programAktivan.Idzida);
            return View(programAktivan);
        }

        // GET: ProgramAktivan/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var programAktivan = await _context.ProgramAktivan
                .Include(p => p.IdzidaNavigation)
                .Include(p => p.IdProgramaNavigation)
                .SingleOrDefaultAsync(m => m.IdprogramAktivan == id);
            if (programAktivan == null)
            {
                return NotFound();
            }

            return View(programAktivan);
        }

        // POST: ProgramAktivan/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var programAktivan = await _context.ProgramAktivan.SingleOrDefaultAsync(m => m.IdprogramAktivan == id);
            _context.ProgramAktivan.Remove(programAktivan);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool ProgramAktivanExists(int id)
        {
            return _context.ProgramAktivan.Any(e => e.IdprogramAktivan == id);
        }
    }
}
