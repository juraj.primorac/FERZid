using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class PovijestPrikazivanjasController : Controller
    {
        private readonly RPPP08Context _context;

        public PovijestPrikazivanjasController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: PovijestPrikazivanjas
        public async Task<IActionResult> Index()
        {
            var rPPP08Context = _context.PovijestPrikazivanja.Include(p => p.IdInstancePrikazivanjaNavigation).Include(p => p.IdProgramaNavigation);
            return View(await rPPP08Context.ToListAsync());
        }

        // GET: PovijestPrikazivanjas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var povijestPrikazivanja = await _context.PovijestPrikazivanja
                .Include(p => p.IdInstancePrikazivanjaNavigation)
                .Include(p => p.IdProgramaNavigation)
                .SingleOrDefaultAsync(m => m.IdPrograma == id);
            if (povijestPrikazivanja == null)
            {
                return NotFound();
            }

            return View(povijestPrikazivanja);
        }

        // GET: PovijestPrikazivanjas/Create
        public IActionResult Create()
        {
            ViewData["IdInstancePrikazivanja"] = new SelectList(_context.PovijestInstanciPrikazivanja, "IdInstancePrikazivanja", "IdInstancePrikazivanja");
            ViewData["IdPrograma"] = new SelectList(_context.Program, "IdPrograma", "ImePrograma");
            return View();
        }

        // POST: PovijestPrikazivanjas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdPrograma,IdInstancePrikazivanja")] PovijestPrikazivanja povijestPrikazivanja)
        {
            if (ModelState.IsValid)
            {
                _context.Add(povijestPrikazivanja);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdInstancePrikazivanja"] = new SelectList(_context.PovijestInstanciPrikazivanja, "IdInstancePrikazivanja", "IdInstancePrikazivanja", povijestPrikazivanja.IdInstancePrikazivanja);
            ViewData["IdPrograma"] = new SelectList(_context.Program, "IdPrograma", "ImePrograma", povijestPrikazivanja.IdPrograma);
            return View(povijestPrikazivanja);
        }

        // GET: PovijestPrikazivanjas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var povijestPrikazivanja = await _context.PovijestPrikazivanja.SingleOrDefaultAsync(m => m.IdPrograma == id);
            if (povijestPrikazivanja == null)
            {
                return NotFound();
            }
            ViewData["IdInstancePrikazivanja"] = new SelectList(_context.PovijestInstanciPrikazivanja, "IdInstancePrikazivanja", "IdInstancePrikazivanja", povijestPrikazivanja.IdInstancePrikazivanja);
            ViewData["IdPrograma"] = new SelectList(_context.Program, "IdPrograma", "ImePrograma", povijestPrikazivanja.IdPrograma);
            return View(povijestPrikazivanja);
        }

        // POST: PovijestPrikazivanjas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdPrograma,IdInstancePrikazivanja")] PovijestPrikazivanja povijestPrikazivanja)
        {
            if (id != povijestPrikazivanja.IdPrograma)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(povijestPrikazivanja);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PovijestPrikazivanjaExists(povijestPrikazivanja.IdPrograma))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdInstancePrikazivanja"] = new SelectList(_context.PovijestInstanciPrikazivanja, "IdInstancePrikazivanja", "IdInstancePrikazivanja", povijestPrikazivanja.IdInstancePrikazivanja);
            ViewData["IdPrograma"] = new SelectList(_context.Program, "IdPrograma", "ImePrograma", povijestPrikazivanja.IdPrograma);
            return View(povijestPrikazivanja);
        }

        // GET: PovijestPrikazivanjas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var povijestPrikazivanja = await _context.PovijestPrikazivanja
                .Include(p => p.IdInstancePrikazivanjaNavigation)
                .Include(p => p.IdProgramaNavigation)
                .SingleOrDefaultAsync(m => m.IdPrograma == id);
            if (povijestPrikazivanja == null)
            {
                return NotFound();
            }

            return View(povijestPrikazivanja);
        }

        // POST: PovijestPrikazivanjas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var povijestPrikazivanja = await _context.PovijestPrikazivanja.SingleOrDefaultAsync(m => m.IdPrograma == id);
           // var povijestInstanciPrikazivanja = await _context.PovijestInstanciPrikazivanja.SingleOrDefaultAsync(m => m.IdInstancePrikazivanja == id);
            //_context.PovijestInstanciPrikazivanja.Remove(povijestInstanciPrikazivanja);
            _context.PovijestPrikazivanja.Remove(povijestPrikazivanja);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PovijestPrikazivanjaExists(int id)
        {
            return _context.PovijestPrikazivanja.Any(e => e.IdPrograma == id);
        }
    }
}
