using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;
using FERZid.ViewModels;

namespace FERZid.Controllers
{
    public class ServisController : Controller
    {
        private readonly RPPP08Context _context;

        public ServisController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: Servis
        public async Task<IActionResult> Index()
        {
            var rPPP08Context = _context.Servis.Include(s => s.IdstatusaNavigation)
                .Include(s => s.IduredajaNavigation)
                .Include(s => s.IdvrsteNavigation)
                .AsNoTracking()
                .OrderBy(s => s.Pocetak);
            return View(await rPPP08Context.ToListAsync());
        }

        // GET: Servis/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var servis = await _context.Servis
                .AsNoTracking()
                .Where(m => m.Idservisa == id)
                .Select(s => new ServisViewModel
                {
                    Idservisa = s.Idservisa,
                    Iduredaja = s.Iduredaja,
                    ImeUredaja = s.IduredajaNavigation.ImeUredaja,
                    Opis = s.Opis,
                    Idstatusa = s.Idstatusa,
                    NazivStatusa = s.IdstatusaNavigation.Naziv,
                    Idvrste = s.Idvrste,
                    NazivVrste = s.IdvrsteNavigation.Naziv,
                    Pocetak = s.Pocetak,
                    Zavrsetak = s.Zavrsetak
                })
                .FirstOrDefaultAsync();

            if (servis == null)
            {
                return NotFound();
            }
            else
            {
                var uloge = await _context.UlogaServisera
                    .Where(s => s.Idservisa == servis.Idservisa)
                    .Select(s => new UlogaServiseraViewModel
                    {
                        Idservisa = s.Idservisa,
                        Idservisera = s.Idservisera,
                        NazivServisera = s.IdserviseraNavigation.Prezime + ", " + s.IdserviseraNavigation.Ime + " (" + s.IdserviseraNavigation.Oib + ")",
                        Uloga = s.Uloga,
                        Zapoceo = s.Zapoceo,
                        Zavrsio = s.Zavrsio
                    })
                    .OrderBy(s => s.NazivServisera)
                    .ToListAsync();

                servis.Uloge = uloge;
            }
            

            return View(servis);
        }

        // GET: Servis/Create
        public IActionResult Create()
        {
            ViewData["Idstatusa"] = new SelectList(_context.StatusServisa, "Idstatusa", "Naziv");
            ViewData["Iduredaja"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja");
            ViewData["Idvrste"] = new SelectList(_context.VrstaServisa, "Idvrste", "Naziv");
            return View();
        }

        // POST: Servis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idservisa,Iduredaja,Opis,Idstatusa,Idvrste,Pocetak,Zavrsetak")] Servis servis)
        {
            if (ModelState.IsValid)
            {
                _context.Add(servis);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["Idstatusa"] = new SelectList(_context.StatusServisa, "Idstatusa", "Naziv", servis.Idstatusa);
            ViewData["Iduredaja"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja", servis.Iduredaja);
            ViewData["Idvrste"] = new SelectList(_context.VrstaServisa, "Idvrste", "Naziv", servis.Idvrste);
            return View(servis);
        }

        // GET: Servis/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var servis = await _context.Servis.SingleOrDefaultAsync(m => m.Idservisa == id);
            if (servis == null)
            {
                return NotFound();
            }
            ViewData["Idstatusa"] = new SelectList(_context.StatusServisa, "Idstatusa", "Naziv", servis.Idstatusa);
            ViewData["Iduredaja"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja", servis.Iduredaja);
            ViewData["Idvrste"] = new SelectList(_context.VrstaServisa, "Idvrste", "Naziv", servis.Idvrste);
            return View(servis);
        }

        // POST: Servis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idservisa,Iduredaja,Opis,Idstatusa,Idvrste,Pocetak,Zavrsetak")] Servis servis)
        {
            if (id != servis.Idservisa)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(servis);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ServisExists(servis.Idservisa))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["Idstatusa"] = new SelectList(_context.StatusServisa, "Idstatusa", "Naziv", servis.Idstatusa);
            ViewData["Iduredaja"] = new SelectList(_context.Uredaj, "Iduredaja", "ImeUredaja", servis.Iduredaja);
            ViewData["Idvrste"] = new SelectList(_context.VrstaServisa, "Idvrste", "Naziv", servis.Idvrste);
            return View(servis);
        }

        // GET: Servis/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var servis = await _context.Servis
                .Include(s => s.IdstatusaNavigation)
                .Include(s => s.IduredajaNavigation)
                .Include(s => s.IdvrsteNavigation)
                .SingleOrDefaultAsync(m => m.Idservisa == id);
            if (servis == null)
            {
                return NotFound();
            }

            return View(servis);
        }

        // POST: Servis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var servis = await _context.Servis.SingleOrDefaultAsync(m => m.Idservisa == id);
            _context.Servis.Remove(servis);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool ServisExists(int id)
        {
            return _context.Servis.Any(e => e.Idservisa == id);
        }
    }
}
