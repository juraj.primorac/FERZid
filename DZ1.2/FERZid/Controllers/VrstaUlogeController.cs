using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FERZid.Models;

namespace FERZid.Controllers
{
    public class VrstaUlogeController : Controller
    {
        private readonly RPPP08Context _context;

        public VrstaUlogeController(RPPP08Context context)
        {
            _context = context;    
        }

        // GET: VrstaUloge
        public async Task<IActionResult> Index(string searchString,string sortOrder,string currentFilter, string errorMessage, string successMessage, int page = 0)
        {

            ViewBag.CurrentSort = sortOrder;

            ViewBag.nazivSortParam = String.IsNullOrEmpty(sortOrder) ? "naziv_desc" : "";


            if (searchString != null)
            {
                page = 0;
            }

            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;



            var uloge = from m in _context.VrstaUloge
                       select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                uloge = uloge.Where(y => y.Naziv.Contains(searchString));
            }

            switch (sortOrder)
            {

                case "naziv_desc":
                    uloge = uloge.OrderByDescending(s => s.Naziv);
                    break;
             
                default:
                    uloge = uloge.OrderBy(s => s.Naziv);
                    break;
            }

            const int pageSize = 3;
            var count = uloge.Count();
            if (count == 0) page = 0;
            else if ((page * pageSize) >= count) --page;
            uloge = uloge.Skip(page * pageSize).Take(pageSize);
            this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
            this.ViewBag.Page = page;

  

            var vrstaUlogeViewModel = new VrstaUlogeViewModel();
            
            vrstaUlogeViewModel.vrsteUloga= await uloge.ToListAsync();

            ViewData["Error"] = errorMessage;
            TempData["Success"] = successMessage;

            return View(vrstaUlogeViewModel);
        }

        // GET: VrstaUloge/Details/5
        public async Task<IActionResult> Details(int? id, int? previousDetail, int? nextDetail, string sortOrder, string currentFilter, int page)
        {
            if (id == null && previousDetail == null && nextDetail == null)
            {
                return NotFound();
            }

            var vrst = from m in _context.VrstaUloge
                             select m;
            List<VrstaUloge> uloge = await vrst.ToListAsync();


            if (previousDetail != null)
            {
                id = uloge.ElementAt((int)previousDetail).IdvrsteUloge;
            }

            if (nextDetail != null)
            {
                id = uloge.ElementAt((int)nextDetail).IdvrsteUloge;
            }

            var vrstaUloge = await _context.VrstaUloge
               .AsNoTracking()
               .Where(m => m.IdvrsteUloge == id)
               .Select(s => new VrstaUlogeViewModel
               {
                   IdvrsteUloge=s.IdvrsteUloge,
                   Naziv =s.Naziv
                })
               .FirstOrDefaultAsync();

            if (vrstaUloge == null)
            {
                return NotFound();
            }
            else
            {
                vrstaUloge.vrsteUloga = uloge;
                int indexAktualni = vrstaUloge.vrsteUloga.FindIndex(k => k.IdvrsteUloge == id);
                ViewBag.IndexAktualni = indexAktualni;
                ViewBag.NumberOfElements = vrstaUloge.vrsteUloga.Count();
            }

            ViewBag.Page = page;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentFilter = currentFilter;

            return View(vrstaUloge);
        }

        // GET: VrstaUloge/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: VrstaUloge/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdvrsteUloge,Naziv")] VrstaUloge vrstaUloge)
        {
            string successMessage = null;

            if (ModelState.IsValid)
            {
                _context.Add(vrstaUloge);
                await _context.SaveChangesAsync();
                successMessage = "Vrsta uloge created: " + vrstaUloge.Naziv;
                return RedirectToAction("Index",new { successMessage=successMessage});
            }
            return View(vrstaUloge);
        }

        // GET: VrstaUloge/Edit/5
        public async Task<IActionResult> Edit(int? id, string sortOrder, string currentFilter, int page, string successMessage)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vrstaUloge = await _context.VrstaUloge.SingleOrDefaultAsync(m => m.IdvrsteUloge == id);
            if (vrstaUloge == null)
            {
                return NotFound();
            }

            ViewBag.Page = page;
            ViewBag.CurrentFilter = currentFilter;
            ViewBag.CurrentSort = sortOrder;

            if (successMessage != null) TempData["Success"] = successMessage;
            return View(vrstaUloge);
        }

        // POST: VrstaUloge/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, string sortOrder, string currentFilter, int page, [Bind("IdvrsteUloge,Naziv")] VrstaUloge vrstaUloge)
        {
            string successMessage = null;

            if (id != vrstaUloge.IdvrsteUloge)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vrstaUloge);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VrstaUlogeExists(vrstaUloge.IdvrsteUloge))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                successMessage = "Vrsta uloge edited: " + vrstaUloge.Naziv;
                return RedirectToAction("Edit", new { page = page, sortOrder = sortOrder, currentFilter = currentFilter, successMessage = successMessage });
            }
            return View(vrstaUloge);
        }

        // GET: VrstaUloge/Delete/5
        public async Task<IActionResult> Delete(int? id, string sortOrder, string currentFilter, int page)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vrstaUloge = await _context.VrstaUloge
                .SingleOrDefaultAsync(m => m.IdvrsteUloge == id);
            if (vrstaUloge == null)
            {
                return NotFound();
            }

            //return View(vrstaUloge);
            return await DeleteConfirmed((int)id, sortOrder, currentFilter, page);
        }

        // POST: VrstaUloge/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, string sortOrder, string currentFilter, int page)
        {
            string errorMessage = null;
            string successMessage = null;

            var vrstaUloge = await _context.VrstaUloge.SingleOrDefaultAsync(m => m.IdvrsteUloge == id);
            _context.VrstaUloge.Remove(vrstaUloge);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                errorMessage = "Vrsta uloge delete failed: " + vrstaUloge.Naziv;
            }

            if (errorMessage == null) successMessage = "Vrsta uloge deleted: " + vrstaUloge.Naziv;
            return RedirectToAction("Index", new { page = page, sortOrder = sortOrder, currentFilter = currentFilter, errorMessage = errorMessage, successMessage = successMessage }); 
        }

        private bool VrstaUlogeExists(int id)
        {
            return _context.VrstaUloge.Any(e => e.IdvrsteUloge == id);
        }
    }
}
