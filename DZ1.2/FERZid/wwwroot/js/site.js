﻿// Write your Javascript code.
$(function () {
    $(document).on('click', '.delete', function () {
        if (!confirm("Želite li obrisati zapis?")) {
            event.preventDefault();
        }
    });
    $(document).on('click', '.update', function () {
        if (!confirm("Ovaj zapis će se promijeniti. Jeste li sigurni?")) {
            event.preventDefault();
        }
    });
});