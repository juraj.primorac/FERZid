﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FERZid.Models
{
    public class UlogaOsobeViewModel
    {

        public int IdulogeOsobe { get; set; }       
        public int? IdvrsteUloge { get; set; }
        public string NazivVrsteUloge { get; set; }
        public int? Idosobe { get; set; }
        public string ImeIPrezimeOsobe { get; set; }
        public int? Idsadržaja { get; set; }
        public string NazivSadrzaja { get; set; }



        public List<UlogaOsobe> ulogeOsoba;
        public SelectList osobe;
        public string osoba { get; set; }
        public SelectList sadrzaji;
        public string sadrzaj { get; set; }
        public SelectList vrsteUloga;
        public string vrstaUloge { get; set; }


    }
}
