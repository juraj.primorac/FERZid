﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FERZid.ViewModels
{
    public class ServiserViewModel
    {
        public int Idservisera { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Oib { get; set; }
        public string Email { get; set; }
        public string Sjediste { get; set; }
        public string Ziroracun { get; set; }
        public int Idbanke { get; set; }
        public string NazivBanke { get; set; }
        public string IbanBanke { get; set; }
        public string SwiftBanke { get; set; }
        public string SjedisteBanke { get; set; }

        public IEnumerable<UlogaServiseraViewModel> Servisi  { get; set; }

        public ServiserViewModel()
        {
            this.Servisi = new List<UlogaServiseraViewModel>();
        }
    }
}
