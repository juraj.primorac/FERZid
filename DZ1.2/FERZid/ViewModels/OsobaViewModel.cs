﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FERZid.Models
{
    public class OsobaViewModel
    {

        public int Idosobe { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        [Display(Name = "OIB")]
        public string Oib { get; set; }

        public virtual ICollection<UlogaOsobe> UlogaOsobe { get; set; }

        public List<Osoba> osobe;
        
    }
}
