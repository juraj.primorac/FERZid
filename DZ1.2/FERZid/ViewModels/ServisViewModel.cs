﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FERZid.ViewModels
{
    public class ServisViewModel
    {
        public int Idservisa { get; set; }
        public int Iduredaja { get; set; }
        public string ImeUredaja { get; set; }
        public string Opis { get; set; }
        public int Idstatusa { get; set; }
        public string NazivStatusa { get; set; }
        public int Idvrste { get; set; }
        public string NazivVrste { get; set; }
        public DateTime Pocetak { get; set; }
        public DateTime Zavrsetak { get; set; }

        public IEnumerable<UlogaServiseraViewModel> Uloge { get; set; }

        public ServisViewModel()
        {
            this.Uloge = new List<UlogaServiseraViewModel>();
        }
    }
}
