﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FERZid.ViewModels
{
    public class UlogaServiseraViewModel
    {
        public int Idservisera { get; set; }
        public string NazivServisera { get; set; }
        public string ImeUredaja { get; set; }
        public int Idservisa { get; set; }
        public string Uloga { get; set; }
        public DateTime Zapoceo { get; set; }
        public DateTime? Zavrsio { get; set; }
    }
}
