﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FERZid.Models
{
    public class FormatViewModel
    {


        public int Idformata { get; set; }
        public string Naziv { get; set; }

        public List<Format> formati;
        public string format { get; set; }
    }
}
