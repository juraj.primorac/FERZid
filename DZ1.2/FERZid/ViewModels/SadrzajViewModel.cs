﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace FERZid.Models
{
    public class SadrzajViewModel
    {


        public int Idsadržaja { get; set; }
        public string Nazivsadržaja { get; set; }
        public int IdvrsteSadržaja { get; set; }
        public string NazivVrsteSadržaja { get; set; }
        public int Idformata { get; set; }
        public string NazivFormata { get; set; }
        public byte[] Podaci { get; set; }
        public long? Veličina { get; set; }
 

        public List<Sadržaj> sadrzaji;
        public SelectList formati;
        public string formatSadrzaja { get; set; }
        public SelectList vrsteSadrzaja;
        public string vrstaSadrzaja { get; set; }


        public IEnumerable<UlogaOsobeViewModel> Uloge { get; set; }
        public Dictionary<int, IEnumerable<UlogaOsobeViewModel>> sadrzajUloge { get; set; }

        public SadrzajViewModel()
        {
            this.Uloge = new List<UlogaOsobeViewModel>();
            this.sadrzajUloge = new Dictionary<int,IEnumerable<UlogaOsobeViewModel>>();
        }


    }
}
