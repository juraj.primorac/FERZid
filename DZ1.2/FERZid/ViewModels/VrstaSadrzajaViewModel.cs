﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FERZid.Models
{
    public class VrstaSadrzajaViewModel
    {

        public int IdvrsteSadržaja { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }



        public virtual ICollection<Sadržaj> Sadržaj { get; set; }

        public List<VrstaSadržaja> vrsteSadrzaja;
    }
}
