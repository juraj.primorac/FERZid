﻿
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FERZid.Models
{
    public class VrstaUlogeViewModel
    {

        public int IdvrsteUloge { get; set; }
        public string Naziv { get; set; }

        public virtual ICollection<UlogaOsobe> UlogaOsobe { get; set; }


        public List<VrstaUloge> vrsteUloga;
        public SelectList vrstaUlogeSelectList;
        public string vrstaUloge { get; set; }
  

    }
}
