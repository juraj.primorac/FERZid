﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FERZid.Models
{
    public partial class Osoba
    {
        public string IP
        {
            get
            {
                return Ime + " " + Prezime;
            }
        }
    }
}
